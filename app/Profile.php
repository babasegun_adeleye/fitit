<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $fillable = [
        'user_id', 'city_id', 'firstname', 'lastname', 'phone', 'address', 'img',
    ];

	public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function billing()
    {
        return $this->hasMany(Billing::class);
    }

    public function add(array $data, $id)
     {
        return Profile::create([
            'user_id' => $id,
            'city_id' => $data['city'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone']
        ]);
        
     }
    //
}
