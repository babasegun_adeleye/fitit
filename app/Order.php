<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\OrderDetails;
use \Cart as Cart;

class Order extends Model
{
	use SoftDeletes;

	protected $fillable = [
        'user_id', 'billing_id', 'order_no', 'order_note', 'order_status', 'order_date', 'total', 'payment', 'quantity',
    ];

	protected $dates = ['deleted_at'];

	public function details()
	{
		return $this->hasMany(OrderDetails::class);
	}

	public function billing()
	{
		return $this->belongsTo(Billing::class);
	}

	public function getOrderDetails($field, $param)
	{
		return Order::with('details')->where($field, $param)->first();
	}


	public function addOrder(array $data, $pay = 'Not paid')
	{
		$suf = 0;
		$dt = Carbon::now();
		$ord = $dt->year.$dt->month.$dt->day.'-'.$dt->hour.$dt->minute.$dt->second;

		$check = $this->checkNum($ord);

		if($check > 0)
		{
			$suf = $suf+1;
		}

		$num = $ord.'-'.$suf;

		$order = Order::create([
		'user_id'=> auth()->user()->id,
		'billing_id'=>$data['address'],
		'quantity' => Cart::count(),
		'order_no'=> $num,
		'order_status'=>'pending',
		'order_date'=> $dt,
		'payment' => $data['payment'],
		'payment_status' => $pay,
		'total'=>Cart::subtotal(2,'.','')
		]);

		if(Cart::count() > 0)
		{
			foreach(Cart::content() as $item)
			{
				$details = new OrderDetails();

				$details->user_id = auth()->user()->id;
				$details->merchant_id = $item->model->merchant_id;
				$details->product_id = $item->id;
				$details->order_id = $order->id;
				$details->order_no = $order->order_no;
				$details->qty = $item->qty;
				$details->total = $item->price * $item->qty;
				$details->status = 0;
				$details->save();
			}
		}

		return $order;
	}

	public function checkNum($ord)
	{
		return Order::where('order_no',$ord)->count();
	}

	public function getCustomerOrders()
	{
		$uid = auth()->user()->id;

		return Order::where('user_id', $uid)->paginate(10);
	}

	public function getCustomerCardOrders()
	{
		return Order::where([
			['user_id', '=', auth()->user()->id],
			['payment', '=', 'card'],
			['order_status', '=', 'pending'],
			['payment_status', '=', 'Not paid'],
		])->latest('id');
	}

	public function updatePayment($id)
	{
		return Order::where('id', $id)->update([
			'payment_status' => 'paid'
		]);
	}
    //
}
