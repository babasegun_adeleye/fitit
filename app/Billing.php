<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Billing extends Model
{
   use SoftDeletes;

   protected $fillable = [
        'user_id', 'city_id', 'profile_id', 'firstname', 'lastname', 'address', 'type', 'phone', 'main',
    ];

   protected $dates = ['deleted_at'];

   public function user()
   {
   		return $this->belongsTo(User::class);
   }

	public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function city()
	{
      return $this->belongsTo(City::class);
  	}

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function myAddress()
    {
    	$uid = auth()->user()->id;

    	return Billing::where('user_id', $uid)->get();
    }

    public function firstByColumn($field, $param)
    {
    	return Billing::where($field, $param)->first();
    }

    public function add(array $data)
    {
    	$user = auth()->user();
    	$pid = $user->profile->id;
    	$main = 'yes';
        $count = Billing::where('user_id', $user->id)->count();

    	if($count > 0)
    	{
    		$main = 'no';
    	}

    	return Billing::create([
    		'user_id' => $user->id,
    		'profile_id' => $pid,
    		'firstname' => $data['firstname'],
    		'lastname' => $data['lastname'],
    		'city_id' => $data['city'],
    		'phone' => $data['phone'],
    		'address'=> $data['add'],
    		'type'=> $data['type'],
    		'main' => $main
    	]);

    }

    public function updateBilling(array $data, $id)
    {
        return Billing::where('id', $id)->update([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'city_id' => $data['city'],
            'phone' => $data['phone'],
            'address'=> $data['add'],
            'type'=> $data['type'],
        ]);
    }

    public function remove($id)
    {
        $bill = Billing::findOrFail($id);
        
        return $bill->delete();
    }
}
