<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'email_token', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
         return $this->hasAnyRole($roles) || null;
     }
        return $this->hasRole($roles) || null;
    }

    public function hasAnyRole($roles)
    {
      return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    /**
    * Check one role
    * @param string $role
    */
    public function hasRole($role)
    {
      return null !== $this->roles()->where('name', $role)->first();
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class);
    }

    public function billing()
    {
      return $this->hasMany(Billing::class);
    }

    public function add(array $data, $code)
    {
        $user =  User::create([
            'username'=> $data['username'],
            'password'=> Hash::make($data['password']),
            'email'=> $data['email'],
            'email_token' => base64_encode( $data['email']),
        ]);

        if($code == 1)
        {
            $user->roles()->attach(Role::where('name', 'customer')->first());
        }
        elseif($code == 2)
        {
            $user->roles()->attach(Role::where('name', 'merchant')->first());
        }
        else{ $user->roles()->attach(Role::where('name', 'admin')->first()); }

        return $user;
    }
}
