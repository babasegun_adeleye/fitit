<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Transaction extends Model
{
	use SoftDeletes;

	protected $fillable = ['reference', 'user_id', 'transId', 'payment_id', 'amount', 'transaction_date'];
	protected $dates = ['deleted_at'];

	public function add(array $array, $pid)
	{
		$dt = new Carbon($array['data']['transaction_date']);
		return Transaction::create([
			'reference' => $array['data']['reference'],
			'user_id' => auth()->user()->id,
			'payment_id' => $pid,
			'transId' => $array['data']['id'],
			'amount' => sprintf('%.2f', $array['data']['amount'] / 100),
			'transaction_date' => $dt->toDateTimeString(),

		]);
	}
    //
}
