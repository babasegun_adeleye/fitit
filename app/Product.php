<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

	protected $fillable = [
        'user_id', 'city_id', 'productsku', 'product_name', 'amount', 'quantity', 'img', 'merchant_id', 'discount', 'product_desc',
    ];

	protected $dates = ['deleted_at'];

	public function categories()
	{
	   return $this->belongsToMany(Category::class);
	}

	public function subcategories()
	{
	   return $this->belongsToMany(Subcategory::class);
	}

	public function merchant()
	{
		return $this->belongsTo(Merchant::class);
	}

	public function city()
	{
		return $this->belongsTo(City::class);
	}

	public function getAll()
	{
		return Product::latest();
	}

	public function getByColumn($col, $param)
	{
		return Product::where($col, $param);
	}

	public function firstByColumn($col, $param)
	{
		return Product::where($col, $param)->firstOrFail();
	}

	public function add(array $data, $img)
	{
		$words = explode(' ', $data['name']);
		$pname = $words[0][0]. $words[0][1].$words[0][2];
        $mid = auth()->user()->merchant->id;
        $sku = strtoupper($pname).$mid.$data['cats'].date('ym');
		$product = Product::create([
			'productsku'=> $sku,
			'product_name'=> $data['name'],
			'product_desc'=> $data['description'],
			'amount'=> $data['price'],
			'quantity'=> $data['quantity'],
			'discount'=> 0,
			'img'=> $img,
			'city_id'=>auth()->user()->merchant->city_id,
			'merchant_id'=> $mid
		]);

		$product->categories()->attach($data['cats']);
		$product->subcategories()->attach($data['subcategory']);

		return $product;
	}

	public function remove($id)
	{
		$product = Product::findOrFail($id);
		
		return $product->delete();
	}

	public function updateProduct(array $data, $id)
	{
		$product = Product::where('id', $id)->update([
			'product_name'=> $data['name'],
			'product_desc'=> $data['description'],
			'amount'=> $data['price'],
			'quantity'=> $data['quantity'],
		]);

		$prod = $this->firstByColumn('id', $id);

		$prod->categories()->sync($data['cats']);
		$prod->subcategories()->sync($data['subcategory']);

		return $product;
	}

	public function updateQty($pid,$qt)
	{
		$prods = $this->firstByColumn('id', $pid);
		$qty = $prods->quantity - $qt;

		return Product::where('id',$pid)->update([
			'quantity' => $qty
		]);
	}



    //
}
