<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $fillable = [
        'city_name', 'country_id', 'status',
    ];

    public function country()
	{    
		return $this->belongsTo(Country::class);
    }

    public function getAll()
    {
        return City::latest();
    }

    public function billings()
    {
        return $this->hasMany(Billing::class);
    }

    public function add(array $data)
    {
        $loc = City::create([
            'city_name' => $data['name'],
            'country_id' => $data['country']
        ]);

        return $loc;
    }

    public function firstByColumn($field, $param)
    {
        return City::where($field, $param)->first();
    }

    public function remove($id)
    {
        $city = $this->firstByColumn('id', $id);
        if($city->status == 1)
        {
            $status = 2;
        }
        else{ $status = 1;}
        return City::where('id', $id)->update([
            'status' => $status,
        ]);
    }

    public function updateCity(array $array, $id)
    {
        return City::where('id',$id)->update([
            'city_name' => $array['name'],
            'country_id' => $array['country']
        ]);
    }
    //
}
