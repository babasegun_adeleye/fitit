<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;

class ImageController extends Controller
{
	public function resizeImage($image)
	{
		$input['name'] = time().'.'.$image->getClientOriginalExtension();

		$destinationPath = public_path('/images/product/thumbnail');
		$img = Image::make($image->getRealPath());
		$img->resize(250, 250);
		$img->save($destinationPath.'/'.$input['name']);

		$destinationPath = public_path('/images/product');
		$img = Image::make($image->getRealPath());
		$img->resize(800, 800);
		$img->save($destinationPath.'/'.$input['name']);

		return $input['name'];
	}

	 public function resizeBanner($image)
	 {
	 	$input['name'] = auth()->user()->merchant->store_name.'.'.$image->getClientOriginalExtension();
	    $destinationPath = public_path('/images/banners');
	     $img = Image::make($image->getRealPath());
	     $img->resize(870, 300);
	     $img->save($destinationPath.'/'.$input['name']);
	     
	     return $input['name'];
	 }
    //
}
