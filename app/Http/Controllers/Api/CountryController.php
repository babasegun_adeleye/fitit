<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CountryResource;
use Illuminate\Support\Facades\Gate;
use App\Country;

class CountryController extends Controller
{
	protected $ctry;

    public function __construct()
    {   
        $this->ctry = new Country();
    }
    
	public function index(Request $request)
	{
        if(Gate::none(['isCustomer','isMerchant']))
        {
            return CountryResource::collection($this->ctry->getAll()->paginate(10));
        }
	}

    public function active()
    {
        if(Gate::none(['isCustomer','isMerchant']))
        {
            return CountryResource::collection($this->ctry->getByColumn('status', 1));
        }
    }

	public function store(Request $request)
    {
        if(Gate::none(['isCustomer','isMerchant']))
        {
            $request->validate([
                'name' => 'bail|required|string|unique:countries,country_name',
                'code' => 'required|unique:countries,country_code',
            ]);

            $req = $this->ctry->add($request->all());
            return response()->json(['message' => 'Country successfully added'], 200);

        }

    }

    public function show($id)
    {
        if(Gate::none(['isMerchant', 'isCustomer']))
        {
            return new CountryResource($this->ctry->firstByColumn('id', $id));
        }
    }

    public function update(Request $request, $id)
    {
        if(Gate::none(['isMerchant', 'isCustomer']))
        {
            $request->validate([
                'name' => 'bail|required|string',
                'code' => 'required',
            ]);

            $ctry = $this->ctry->firstByColumn('id', $id);

            if($ctry->country_name  != $request->name && $ctry->country_code != $request->code)
            {
                $request->validate([
                    'name' => 'string|unique:countries,country_name',
                    'code' => 'unique:countries,country_code',
                ]);
            }

            elseif($ctry->country_name != $request->name && $ctry->country_code == $request->code)
            {
                $request->validate([
                    'name' => 'string|unique:countries,country_name',
                ]);
            }
            else{ 
                $request->validate([
                    'code' => 'unique:countries,country_code',
                ]);
            }
        
            $result = $this->ctry->updateCountry($request->all(),$id);

            return response()->json('Country successfully updated',200);
        }
    }

    public function destroy($id)
    {
        if(Gate::none(['isMerchant', 'isCustomer']))
        {
            $del = $this->ctry->remove($id);
            return response()->json('Country status updated');
        }
    }
    //
}
