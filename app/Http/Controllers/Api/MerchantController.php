<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\MerchantResource;
use App\Http\Controllers\ImageController;
use App\Merchant;
use App\Image;

class MerchantController extends Controller
{
	protected $mer;
	protected $image;
	protected $images;
	protected $path;

	public function __construct()
	{
		$this->mer = new Merchant();
		$this->image = new Image();
		$this->images = new ImageController();
		$this->path = public_path('/images/banners/'); 
	}

	public function details()
	{
		if(Gate::none(['isCustomer','isAdmin']))
		{
			$id = auth()->user()->merchant->id;

			return new MerchantResource($this->mer->getData('id', $id));

		}
	}

	public function update(Request $request, $id)
	{
		if(Gate::none(['isCustomer','isAdmin']))
		{
			$request->validate([
                'name' => 'bail|required|min:5',
                'url' => 'required|min:5',
                'desc' => 'required|min:10',
                'address' => 'required',
                'city'=>'required',
                'phone' => 'required|numeric',
            ]);
            
            $check = $this->mer->getData('id', $id);

            if($check->store_name != $request->get('name'))
            {
            	$name = $this->mer->getData('store_name', $request->get('name'));
            	if(count($name) > 0)
            	{
            		return response()->json([
            			'name'=> 'Store Name has already been taken',
            		], 422);
            	}
            }

            if($check->storeurl != $request->get('url'))
            {
            	$url = $this->mer->getData('storeurl', $request->get('url'));
            	if(count($url) > 0)
            	{
            		return response()->json([
            			'url'=> 'Store URL has already been taken',
            		], 422);
            	}
            }
            
			if(!preg_match("/^[a-z0-9-]+$/", $request->get('url')))
			{
				return response()->json([
					'url'=> 'Store URL can only contain lowercase letters, numbers, or hyphen ',
				], 422);
			}

			$result = $this->mer->updateDetails($request->all(),$id);

			return response()->json('Store details updated', 200);
		}
	}

	public function changeBanner(Request $request)
	{
		if(Gate::none(['isCustomer','isAdmin']))
		{
			$request->validate([
				'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
			]);

			$id = auth()->user()->merchant->id;

			$check = $this->mer->getData('id', $id);

			if($check->img != null)
			{
				$this->image->removeBanner($this->path, $check->img);
			}

			$img = $this->images->resizeBanner($request->image);

			$result = $this->mer->updateBanner($img, $id);

			return response()->json('Banner successfully updated', 200);


		}
	}
    //
}
