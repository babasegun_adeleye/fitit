<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	public function changePassword(Request $request)
	{
		$request->validate([
			'current' => 'bail|required|min:8',
			'password' => 'required|min:8|confirmed',
		]);

		if(!(Hash::check($request->current, auth()->user()->password)))
		{
			return response()->json(['error'=>'Your current password does not match our records'], 422);
		}

		if(strcmp($request->current, $request->password) == 0)
		{
			return response()->json(['error'=>'Your current and new password are the same'], 422);
		}

		$user = auth()->user();

		$user->password = Hash::make($request->password);
		$user->save();

		return response()->json('Password successfully changed',200);

	}

	
    //
}
