<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SubcategoryResource;
use Illuminate\Support\Facades\Gate;
use App\Subcategory;

class SubcategoryController extends Controller
{
    protected $sub;

    public function __construct()
    {
        
        $this->sub = new Subcategory();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SubcategoryResource::collection($this->sub->getAll()->paginate(10));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'bail|required|unique:subcategories,sub_name',
            'slug' => 'required|unique:subcategories,slug',
            'category' => 'required|numeric',
        ]);

        $req = $this->sub->add($request->all());

        return response()->json(['message' => 'Subcategory successfully added'], 200);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Gate::none(['isMerchant', 'isCustomer']))
        {
            return new SubcategoryResource($this->sub->firstByColumn('id', $id));
        }
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       if(Gate::none(['isMerchant', 'isCustomer']))
        {
            $request->validate([
                'name' => 'bail|required|string',
                'slug' => 'required',
                'category' => 'required|numeric',
            ]);

            $validate = $this->validatData($request->all(), $id);

            if($validate == 1)
            {
                return response()->json([
                    'name' => 'Change name, slug or category value',
                    'slug' => 'Change name, slug or category value',
                    'category' => 'Change name, slug or category value'
            ], 422);
            }
            elseif($validate == 2)
            {
                return response()->json([
                    'name' => 'name has already been taken'
                ], 422);
            }
             elseif($validate == 3)
            {
                return response()->json([
                    'slug' => 'slug has already been taken'
                ], 422);
            }

            else{
                $result = $this->sub->updateSubcat($request->all(),$id);
            }


            return response()->json('Subcategory successfully updated', 200);
        } //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::none(['isMerchant', 'isCustomer']))
        {
            $del = $this->sub->remove($id);
            return response()->json('Subcategory status updated');
        }
        //
    }

    private function validatData(array $data, $id)
    {
        $get = $this->sub->firstByColumn('id', $id);
        if($get->sub_name == $data['name'] && $get->slug == $data['slug'] && $get->category_id == $data['category'])
        {
            return 1;
        }

        if($get->sub_name != $data['name'])
        {
            if($this->sub->firstByColumn('sub_name', $data['name']) != null)
            {
                return 2;
            }   
        }

        if($get->slug != $data['slug'])
        {
            if($this->sub->firstByColumn('slug', $data['slug']) != null)
            {
                return 3;
            } 
        }

        return 0;
    }
}
