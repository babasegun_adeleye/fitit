<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\SubcategoryResource;
use App\Category;

class CategoryController extends Controller
{
    protected $cat;

    public function __construct()
    {   
        $this->cat = new Category();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return CategoryResource::collection($this->cat->getAll()->paginate(10));  //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if(Gate::none(['isCustomer','isMerchant']))
         {
            $request->validate([
                'name' => 'bail|required|string|unique:categories,cat_name',
                'slug' => 'required|unique:categories,slug',
            ]);

            $req = $this->cat->add($request->all());

            return response()->json(['message' => 'Category successfully added'], 200);
         }
        
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new CategoryResource($this->cat->getCategory('id',$id));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::none(['isCustomer','isMerchant']))
        {
            $request->validate([
                'name' => 'bail|required|string',
                'slug' => 'required',
            ]);

            $cat = $this->cat->getCategory('id', $id);

            if($cat->cat_name  != $request->name && $cat->slug != $request->slug)
            {
                $request->validate([
                    'name' => 'string|unique:categories,cat_name',
                    'slug' => 'unique:categories,slug',
                ]);
            }

            elseif($cat->cat_name != $request->name && $cat->slug == $request->slug)
            {
                $request->validate([
                    'name' => 'string|unique:categories,cat_name',
                ]);
            }
            else{ 
                $request->validate([
                    'slug' => 'unique:categories,slug',
                ]);
            }
        
            $result = $this->cat->updateCategory($request->all(),$id);

            return response()->json('Category successfully updated',200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::none(['isCustomer','isMerchant']))
        {
            $del = $this->cat->remove($id);
            return response()->json('Category status updated');
        } 
    }

    public function subCats($id)
    {
        return SubcategoryResource::collection($this->cat->getSubcat($id));
    }
}
