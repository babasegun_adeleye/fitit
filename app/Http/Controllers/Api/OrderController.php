<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\DetailResource;
use Illuminate\Support\Facades\Gate;
use App\Order;
use App\OrderDetails;

class OrderController extends Controller
{
	protected $ord, $detail;

	public function __construct()
	{
		$this->ord = new Order();
		$this->detail = new OrderDetails();
	}

	public function customerOrders()
	{
		if(Gate::none(['isAdmin', 'isMerchant']))
		{
			return OrderResource::collection($this->ord->getCustomerOrders());
		}
	}

	public function merchantOrders()
	{
		if(Gate::none(['isAdmin', 'isCustomer']))
		{
			$mid = auth()->user()->merchant->id;
			return DetailResource::collection($this->detail->getByColumn('merchant_id', $mid)->paginate(10));
		}
	}

	public function details($num)
	{
		if(Order::where('order_no', $num)->exists())
		{
			return new OrderResource($this->ord->getOrderDetails('order_no', $num));
		}
	}
    //
}
