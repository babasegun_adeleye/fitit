<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Resources\ProductResource;
use App\Product;

class ProductController extends Controller
{

    protected $prod;
    protected $images;

    public function __construct()
    {
        $this->prod = new Product();
        $this->images = new ImageController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::none(['isAdmin', 'isCustomer']))
        {
            $mid = auth()->user()->merchant->id;
            $query = $this->prod->getByColumn('merchant_id', $mid)->paginate(10);
        }
        return ProductResource::collection($query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'name' => 'bail|required',
            'description' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'subcategory' => 'required|numeric',
            'cats' => 'required|numeric',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

         $img = $this->images->resizeImage($request->file('image'));

         $result = $this->prod->add($request->all(),$img);

          return response()->json(['message' => 'Product successfully created'], 200);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ProductResource($this->prod->firstByColumn('id', $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $request->validate([
            'name' => 'bail|required',
            'description' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'subcategory' => 'required|numeric',
            'cats' => 'required|numeric',
        ]);

       // $image = $this->prod->firstByColumn('id', $id);
       // $img = $request->file('image');
        
       //  if($img != $image->img)
       //  {
       //      $img = $this->images->resizeImage($request->file('image'));
       //  } 

        $result = $this->prod->updateProduct($request->all(),$id);

        return response()->json('Product updated successflly', 200);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::none(['isCustomer']))
        {
            $result = $this->prod->remove($id);

            return response()->json('Product successfully deleted',200);
        }
        //
    }
}
