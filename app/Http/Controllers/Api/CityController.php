<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CityResource;
use Illuminate\Support\Facades\Gate;
use App\City;

class CityController extends Controller
{
	protected $cty;

    public function __construct()
    {
        
        $this->cty = new City();
    }

    public function index(Request $request)
    {
    	return CityResource::collection($this->cty->getAll()->paginate(10));
    }

    public function store(Request $request)
    {
        if(Gate::none(['isMerchant', 'isCustomer']))
        {
            $request->validate([
                'name' => 'bail|required|alpha|unique:cities,city_name',
                'country' => 'required|numeric',
            ]);

            $req = $this->cty->add($request->all());

            return response()->json(['message' => 'City successfully added'], 200);  
        }
    	
    }

    public function show($id)
    {
        if(Gate::none(['isMerchant', 'isCustomer']))
        {
            return new CityResource($this->cty->firstByColumn('id', $id));
        }
    }

    public function update(Request $request, $id)
    {
        if(Gate::none(['isMerchant', 'isCustomer']))
        {
            $request->validate([
                'name' => 'bail|required|string',
                'country' => 'required|numeric',
            ]);

            $validate = $this->validatData($request->all(), $id);

            if($validate == 1)
            {
                return response()->json([
                    'name' => 'Change name or country value',
                    'country' => 'Change name or country value'
            ], 422);
            }
            elseif($validate == 2)
            {
                return response()->json([
                    'name' => 'name has already been taken'
                ], 422);
            }
            else{
                $result = $this->cty->updateCity($request->all(),$id);
            }


            return response()->json('City successfully updated', 200);
        }
    }

    public function destroy($id)
    {
        if(Gate::none(['isMerchant', 'isCustomer']))
        {
            $del = $this->cty->remove($id);
            return response()->json('City status updated');
        }
    }

    public function cities()
    {
        return CityResource::collection($this->cty->getAll()->get());
    }

    private function validatData(array $data, $id)
    {
        $get = $this->cty->firstByColumn('id', $id);
        if($get->city_name == $data['name'] && $get->country_id == $data['country'])
        {
            return 1;
        }

        if($get->city_name != $data['name'])
        {
            if($this->cty->firstByColumn('city_name', $data['name']) != null)
            {
                return 2;
            }   
        }

        return 0;
    }
    //
}
