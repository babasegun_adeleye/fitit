<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\BillingResource;
use App\Billing;

class BillingController extends Controller
{
    protected $bill;

    public function __construct()
    {
        $this->bill = new Billing();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::none(['isMerchant', 'isAdmin']))
        {
            return BillingResource::collection($this->bill->myAddress());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required|alpha',
            'lastname' => 'required|alpha',
            'phone' => 'required|numeric',
            'add'=>'required',
            'type'=>'required',
            'city'=>'required|numeric',
        ]);

        $query = $this->bill->add($request->all());

        return response()->json(['message' => 'Address successfully added'], 200);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new BillingResource($this->bill->firstByColumn('id', $id));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'firstname' => 'required|alpha',
            'lastname' => 'required|alpha',
            'phone' => 'required|numeric',
            'add'=>'required',
            'type'=>'required',
            'city'=>'required|numeric',
        ]);

        $query = $this->bill->updateBilling($request->all(), $id);

        return response()->json('Address successfully updated', 200);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $del = $this->bill->remove($id);
         return response()->json('address status deleted');
        //
    }

    public function updateMain($id)
    {

        $uid = auth()->user()->id;

        $remove = Billing::where([
            ['user_id', '=', $uid],
            ['main', '=', 'yes'],
        ])->update(['main' => 'no']);
       

        $bill = Billing::where('id', $id)->update(['main' => 'yes']);

        return response()->json('Address successfully updated', 200);
    }
}
