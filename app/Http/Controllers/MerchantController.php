<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Merchant;

class MerchantController extends Controller
{
   protected $mer;

   public function __construct()
   {
   	$this->mer = new Merchant();
   }

   public function store($store)
   {

   	  $check = $this->mer->storeExists($store);

   	  if($check)
   	  {
   	  		$thecat = array();
			$catname = array();
   	  		$data = $this->mer->getData('storeurl', $store);
			$prods = $data->products()->orderBy('id', 'desc')->paginate(12);
			$myprod = $data->products()->get();

			foreach ($myprod as $value) {
				$getcat = $value->categories->first();
				$thename = $getcat->categoryName;
				if(!in_array($thename, $catname))
				{
					array_push($catname, $thename);
					array_push($thecat, $value->categories->first());
				}
			}
				
			unset($catname);		
		
			return view('merchant.store',compact('data','prods','thecat'));
   	  }
   	  else{ 
   	  	return view('errors.404');
   	  }
   }

   public function OtherProducts($mid, $pid)
   {
   	  return $this->mer->getOtherProducts($mid, $pid);
   }
}
