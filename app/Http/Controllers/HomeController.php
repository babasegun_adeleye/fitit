<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\MerchantController;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    protected $prod, $cat, $merc;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->prod = new ProductController();
        $this->cat = new CategoryController();
        $this->merc = new MerchantController();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = $this->prod->index();
        return view('home', compact('products'));
        
    }

    public function showCart()
    {
        return view('cart');
    }

    public function checkout()
    {
         if(Gate::none(['isCustomer']))
         {
            return redirect()->route('home')->with('message', 'Please login using a customer account');
         }

        $address = auth()->user()->billing()->get();

        return view('checkout', compact('address'));
    }

    public function showProduct($slug)
    {
        $data = $this->prod->getProduct($slug);
        $mid = $data->merchant_id;
        $prod = $this->merc->OtherProducts($mid,$data->id);

        return view('product.index', compact('data','prod'));
    }

    public function search(Request $request)
    {
        if($request->category == null || $request->category == 'all' && $request->search == null)
        {
            $q = 'All Categories';
            $search = $this->prod->productCategories();
        }
        elseif($request->category != 'all' && $request->search == null)
        {
            $cat = $this->cat->getCategory('slug', $request->category);
            if($cat != null)
            {
                $search = $this->cat->getCategoryProducts($cat->id)->appends(['category' => $cat->slug]);
            }
            else{ $search = 0; }
            $q = 'Category: '.$cat->cat_name;
        }
        elseif($request->category == 'all' && $request->search != null)
        {
            $q = 'All Categories for: '.$request->search;
            $search = $this->prod->searchProduct($request->search);
        }
        elseif($request->category != 'all' && $request->search != null)
        {
            if(($cat = $this->cat->getCategory('slug', $request->category)) != null)
            {
                $search = $this->prod->searchCategory($cat->id, $request->search)->appends([
                    'category' => $request->category,
                    'search' => $request->search
                ]);
            }
            else{ $search = 0; }
            $q = 'Category: '.$cat->cat_name.' for: '.$request->search;
        }

        return view('search.index', compact('search','q'));
    }

    public function category($slug)
    {
        $cat = $this->cat->getCategory('slug', $slug);

        if(count($cat) > 0)
        {
            $id = $cat->id;
            $name = $cat->cat_name;
            return view('search.category', compact('id','name'));
        }
        else{
            return view('errors.404');
        }
    }

    public function subcategory($slug, $sub)
    {
        $cat = $this->cat->getSubcategory($sub);

        if(count($cat) > 0)
        {
            $id = $cat->id;
            $name = $cat->sub_name;

            return view('search.subcat', compact('id','name'));
        }
        else{
            return view('errors.404');
        }
    }

    public function mail()
    {
        return view('test');
    }
}
