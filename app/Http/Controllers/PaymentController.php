<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Payment;
use App\Product;
use App\Transaction;
use App\Mail\OrderMail;
use Paystack;
use Mail;
use \Cart as Cart;

class PaymentController extends Controller
{
    protected $ord, $trans, $pay, $prod;

    public function __construct()
    {
        $this->ord = new Order();
        $this->trans = new Transaction();
        $this->pay = new Payment();
        $this->prod = new Product();
    }

    public function show()
    {
        $page = 'payment.index';
        $count = $this->ord->getCustomerCardOrders()->count();

        if($count < 1)
        {
            $page = 'home';
        }

        return view($page);
    }

	/**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();
        $msg = '';

        //dd($paymentDetails);

        if($paymentDetails['status'] == true)
        {
            $type = 'card';
            //$stat = $this->ord->getCustomerCardOrders()->first();
            $ord = $this->ord->updatePayment(session('order_id'));
            $pay = $this->pay->add($paymentDetails, session('order_id'), $type);
            $trans = $this->trans->add($paymentDetails, $pay->id);
            $order = Order::find(session('order_id'));
            Mail::to(auth()->user()->email)->send(new OrderMail($order));
            foreach(Cart::content() as $item)
                {
                    $result = $this->prod->updateQty($item->model->id, $item->qty);
                    Cart::remove($item->rowId);
                }
            session()->forget('order_id');
            session()->forget('order_no');
            $msg = 'Your payment was successful';

        }
        else{ $msg = 'There was an error with your payment, please try again'; }
       
       return view('payment.status', compact('msg'));
    }
    //
}
