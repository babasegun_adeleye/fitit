<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\BillingResource;
use App\Http\Resources\CartResource;
use Illuminate\Support\Facades\Gate;
use Config;
use Mail;
use App\Mail\OrderMail;
use App\Order;
use App\Billing;
use App\Product;
use \Cart as Cart;


class CartController extends Controller
{
    protected $ord, $prod, $bill;

    public function __construct()
    {
        $this->ord = new Order();
        $this->prod = new Product();
        $this->bill = new Billing();
    }

	public function clear()
    {
        Cart::destroy();

        return redirect()->route('cart.index');
    }
    
	public function store(Request $request)
    {

        if(Cart::count() > 0)
            {
                foreach(Cart::content() as $item)
                    {
                        if($item->model->merchant_id != $request->merchant)
                        {
                            return redirect()->back()->with('warning','You cannot buy from multiple shops at the same time!');
                        }
                    }
            }


        Cart::add($request->id,$request->name,$request->quantity,$request->price)->associate('App\Product');

        return redirect()->back()->with('message','Product added to cart!');
    }

     public function update(Request $request, $id)
    {
        Cart::update($id,$request->quantity);

        return redirect()->route('cart.index')->with('message','Product has been updated!');
    }

    public function destroy($id)
    {
        Cart::remove($id);

        return redirect()->back()->with('message','Product deleted from cart!');;
    }

    public function order(Request $request)
    {
        if(Gate::none(['isCustomer']))
        {
            return redirect()->route('home')->with('message', 'Please login using a customer account');
        }

        $request->validate([
        'payment' => 'required',
        'address' => 'required|numeric',
        ]);

        if(Cart::count() > 0)
        {
            $query = $this->ord->addOrder($request->all());
            session(['order_id' => $query->id]);
            session(['order_no' => $query->order_no]);
            if($request->payment == 'cash')
            {
                $order = Order::find(session('order_id'));
                Mail::to(auth()->user()->email)->send(new OrderMail($order));
                foreach(Cart::content() as $item)
                {
                    $result = $this->prod->updateQty($item->model->id, $item->qty);
                    Cart::remove($item->rowId);
                }
                session()->forget('order_id');
                session()->forget('order_no');
            }

            return response()->json(
                [
                    'message' => 'Order successful'
                ], 200);
        }
        else{ return response()->json(['warning' => 'No item in cart']); }
        
       
    }

    public function checkout()
    {
          $cart = [];
         if(Cart::count() > 0)
         {
            $i = 0;
            foreach(Cart::content() as $item)
            {
                $cart[$i]['rowId'] = $item->rowId;
                $cart[$i]['id'] = $item->id;
                $cart[$i]['name'] = $item->name;
                $cart[$i]['qty'] = $item->qty;
                $cart[$i]['price'] = $item->price;
                $cart[$i]['subtotal'] = $item->subtotal;
                $i++;
            }
         }
         return view('checkout',compact('cart'));
    }

    public function billings()
    {
    
        if(Gate::none(['isMerchant', 'isAdmin']))
        {

            return BillingResource::collection($this->bill->myAddress());
        }
    }

    //
}
