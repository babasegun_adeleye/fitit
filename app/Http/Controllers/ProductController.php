<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Merchant;
use App\Category;

class ProductController extends Controller
{
      protected $product;

      public function __construct()
      {
      	$this->product = new Product();
      }

      public function index()
      {
      	return $this->product->getAll()->paginate(20);
      }

      public function getProduct($slug)
      {
      	return Product::where('productsku', $slug)->firstOrFail();
      }

      public function productCategories()
      {
        return Product::with('categories')->paginate(20);
      }

      public function searchProduct($search)
      {
         $id = array();
         $product = Product::where('product_name', 'LIKE', "%$search%")->exists();
         $mer = Merchant::where('store_name', 'LIKE', "%$search%")->exists();
         if($product)
         {
            $result = Product::where('product_name', 'LIKE', "%$search%")->paginate(20)->appends(['search' => $search]);
         }
         elseif($mer) {
            $merc = Merchant::where('store_name', 'LIKE', "%$search%")->get();
            foreach($merc as $item)
            {
               array_push($id, $item->id);
            }

            $result = Product::whereIn('merchant_id', $id)->paginate(20)->appends(['search' => $search]);
            unset($id);
         }
         else{
            $result = Product::latest()->paginate(20)->appends(['search' => $search]);
         }

         return $result;
      }

      public function searchCategory($cat, $search)
      {
         $product = Product::where('product_name', 'LIKE', "%$search%")->exists();
         if($product)
         {
            $result = Category::find($cat)->products()->where('product_name', 'LIKE', "%$search%")->paginate(20);
            if(count($result) < 1)
            {
               $result = Product::where('product_name', 'LIKE', "%$search%")->paginate(20);
            }
         }
         else{
            $result = Category::find($cat)->products()->paginate(20);
         }

         return $result;
      }

}
