<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\SubcategoryResource;
use App\Category;
use App\Subcategory;

class CategoryController extends Controller
{
	protected $cat,$sub;

	public function __construct()
	{
		$this->cat = new Category();
		$this->sub = new Subcategory();
	}

	public function getCategories(Request $request)
	{
		$category  = $this->cat->getAll()->get();

		return view('', compact('category'));
	}

	public function getCategoryProducts($cid)
	{
		return Category::find($cid)->products()->paginate(20);
	}

	public function getCategory($field,$cat)
	{
		return Category::where($field,$cat)->first();
	}

	public function getSubcategory($sub)
	{
		return $this->sub->firstByColumn('slug', $sub);
	}

	public function products($id)
	{
		$prod = $this->getCategoryProducts($id);

		return ProductResource::collection($prod);
	}

	public function subProducts($id)
	{
		$prod = Subcategory::find($id)->products()->paginate(20);

		return ProductResource::collection($prod);
	}

	public function list($id)
	{
		$cat = Category::whereNotIn('id', [$id])->get();

		return CategoryResource::collection($cat);
	}

	public function subList($id)
	{
		$cat = Subcategory::whereNotIn('id', [$id])->get();

		return SubcategoryResource::collection($cat);
	}

	public function category($id)
	{
		$sub = $this->sub->firstByColumn('id', $id);

		return new CategoryResource($sub->category);
	}
    //
}
