<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Mail\RegisterEmail;
use Carbon\Carbon;
use Mail;
use App\City;
use App\Profile;
use App\User;
use App\Merchant;

class RegistrationController extends Controller
{
	protected $cty;
	protected $user;
	protected $mer;

	public function __construct()
	{
		$this->cty = new City();
		$this->user = new User();
		$this->mer = new Merchant();
	}
	public function index()
	{
		$data = $this->cty->getAll()->get();
		return view('auth.register', compact('data'));
	}

	public function store(Request $request)
	{
		$request->validate([
		'username' => 'required|min:6|unique:users,username',
		'firstname' => 'required|alpha',
		'lastname' => 'required|alpha',
		'password' => 'required|confirmed|min:8',
		'phone' => 'required|numeric',
		'email' => 'required|email|unique:users,email',
		'city'=>'required|numeric',
		]);

		if($this->verifyUsername($request->get('username')) == false)
		{
			$errors = 'username can only contain lowercase letters, numbers, or hyphen';
			return redirect()->back()
                    ->withInput($request->input())
                    ->withErrors($errors);
		}

		$profiles =  new Profile();

		event(new Registered($result = $this->user->add($request->all(),1)));

		if($result)
		{	
			$results = $profiles->add($request->all(),$result->id);
		}

		Mail::to($request->email)->send(new RegisterEmail($result));

		return redirect()->back()->with('message', 'Registration successful, a verification link has been sent to your email');
		
	}

	public function shop(Request $request)
	{
		$data = $this->cty->getAll()->get();

		return view('auth.sell',compact('data'));
	}

	public function sell(Request $request)
	{
		$request->validate([
		'storename' => 'required|min:5|unique:merchants,store_name',
		'surl' => 'required|min:5',
		'username' => 'required|min:6|unique:users,username',
		'desc' => 'required|min:10',
		'address' => 'required',
		'password' => 'required|confirmed|min:8',
		'email' => 'required|email|unique:users,email',
		'city'=>'required',
		'phone' => 'required|numeric',
		]);

		$url = $this->mer->getData('storeurl', $request->get('surl'));
		if(count($url) > 0)
		{
			$errors = 'Store URL has already been taken';
			return redirect()->back()
                    ->withInput($request->input())
                    ->withErrors($errors);
		}

		if(!preg_match("/^[a-z0-9-]+$/", $request->get('surl')))
		{
			$errors = 'Store URL can only contain lowercase letters, numbers, or hyphen ';
			return redirect()->back()
                    ->withInput($request->input())
                    ->withErrors($errors);
		}

		if($this->verifyUsername($request->get('username')) == false)
		{
			$errors = 'username can only contain lowercase letters, numbers, or hyphen';
			return redirect()->back()
                    ->withInput($request->input())
                    ->withErrors($errors);
		}

		event(new Registered($result = $this->user->add($request->all(),2)));

		if($result)
		{
			
			$results = $this->mer->add($request->all(),$result->id);
		}

		Mail::to($request->email)->send(new RegisterEmail($result));

		return redirect()->back()->with('message', 'Registration successful, a verification link has been sent to your email');
	}

	public function verify($email, $token)
	{
		$check = User::where([
			['email', '=', $email],
			['email_token', '=', $token],
		])->exists();

		if(!$check)
		{
			return redirect()->route('home')->with('warning', 'We could not find the details in our records');
		}

		$result = User::where('email',$email)->update([
			'email_token' => null,
			'email_verified_at' => Carbon::now()->toDateTimeString(),
			'active' => 1
		]);

		return redirect()->route('login')->with('message', 'Email verified, you can now Login');
	}

	private function verifyUsername($uname)
	{
       if(!preg_match("/^[a-z0-9_-]+$/", $uname))
       {
       	 return false;
       }
       else
       {
       	 return true;
       }
	}
    //
}
