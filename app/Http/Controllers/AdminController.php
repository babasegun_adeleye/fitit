<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
	public function index()
	{
		if(auth()->user()->hasRole('admin'))
		{
			$user = 'admin';
		}
		elseif(auth()->user()->hasRole('merchant'))
		{
			$user = 'merchant';
		}
		else{ $user = 'customer'; }
		return view('dashboard',compact('user'));
	}
    //
}
