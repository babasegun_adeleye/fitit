<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->country_name,
            'code' => $this->country_code,
            'status' => $this->status,
            'cities' => CityResource::collection($this->whenLoaded('cities')),
        ];
    }
}
