<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubcategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sub_name' => $this->sub_name,
            'slug' => $this->slug,
            'status' => $this->status,
            'category' => new CategoryResource($this->category),
            'total' => count($this->products),
        ];
    }
}
