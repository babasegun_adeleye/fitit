<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->created_at,
            'number' => $this->order_no,
            'status' => $this->order_status,
            'qty' => $this->qty,
            'total' => $this->total,
            'product' => new ProductResource($this->product),
            'order' => new OrderResource($this->order)
        ];
    }
}
