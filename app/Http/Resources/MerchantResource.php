<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MerchantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->store_name,
            'url' => $this->storeurl,
            'desc' => $this->description,
            'city' => $this->city_id,
            'phone' => $this->phone,
            'address' => $this->address,
        ];
    }
}
