<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->cat_name,
            'slug' => $this->slug,
            'status' => $this->status,
            'sub' => SubcategoryResource::collection($this->whenLoaded('subcategory')),
            'total' => count($this->products),
        ];
    }
}
