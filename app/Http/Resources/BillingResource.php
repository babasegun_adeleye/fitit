<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BillingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'phone' => $this->phone,
            'type' => $this->type,
            'main' => $this->main,
            'address'=> $this->address,
            'city' => new CityResource($this->city),
            'orders' => OrderResource::collection($this->whenLoaded('orders')),
        ];
    }
}
