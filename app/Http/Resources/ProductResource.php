<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->productsku,
            'name' => $this->product_name,
            'city' => new CityResource($this->city),
            'desc' => $this->product_desc,
            'price' => $this->amount,
            'quantity' => $this->quantity,
            'image' => $this->img,
            'merchant' => $this->merchant_id,
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'subcategories' => SubcategoryResource::collection($this->whenLoaded('subcategories'))

        ];
    }
}
