<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->order_date,
            'number' => $this->order_no,
            'status' => $this->order_status,
            'qty' => $this->quantity,
            'type' => $this->payment,
            'total' => $this->total,
            'detail' => DetailResource::collection($this->whenLoaded('details')),
            'billing' => new BillingResource($this->billing),
        ];
    }
}
