<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'cat_name', 'slug',
    ];

    public function subcategory()
    {
       return $this->hasMany(Subcategory::class)->orderBy('sub_name', 'asc');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

	public function getAll()
	{
		return Category::latest();
	}

	public function orderedCategory()
    {
        return Category::orderBy('cat_name', 'asc')->get();
    }

    public function categoryWithSub()
    {
        return Category::with('subcategory')->where('status', 1)->get();
    }

    public function add(array $array)
    {
        return Category::create([
            'cat_name' => $array['name'],
            'slug' => $array['slug'],
        ]);
    }

    public function getSubcat($id)
    {
        return Category::find($id)->subcategory()->get();
    }

    public function getCategory($param, $value)
    {
        return Category::where($param, $value)->first();
    }

    public function remove($id)
    {
        $cat = $this->getCategory('id', $id);
        if($cat->status == 1)
        {
            $status = 2;
        }
        else{ $status = 1;}
        return Category::where('id', $id)->update([
            'status' => $status,
        ]);
    } 

    public function updateCategory(array $data, $id)
    {
        return Category::where('id',$id)->update([
            'cat_name' => $data['name'],
            'slug' => $data['slug'],
        ]);
    }
    //
}
