<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
	public function order()
	{
		return $this->belongsTo(Order::class);
	}

	public function merchant()
	{
		return $this->belongsTo(Merchant::class);
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	public function getByColumn($field, $param)
	{
		return OrderDetails::where($field, $param);
	}

	public function getDetails($num)
	{
		return OrderDetails::with('order')->where('order_no', $num)->get();
	}
    //
}
