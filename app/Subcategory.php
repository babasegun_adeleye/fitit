<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
	protected $fillable = [
        'sub_name', 'category_id', 'slug',
    ];

	public function category()
	{    
		return $this->belongsTo(Category::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function subcategoryWithCat()
    {
    	return Subcategory::with('category')->get();
    }

    public function orderedSubcategory()
    {
    	return Subcategory::orderBy('sub_name', 'asc')->get();
    }

    public function getAll()
	{
		return Subcategory::latest();
	}

	public function add(array $array)
    {
        return Subcategory::create([
            'sub_name' => $array['name'],
            'category_id' => $array['category'],
            'slug' => $array['slug'],
        ]);
    }

    public function firstByColumn($field, $param)
    {
        return Subcategory::where($field, $param)->first();
    }

    public function remove($id)
    {
        $sub = $this->firstByColumn('id', $id);
        if($sub->status == 1)
        {
            $status = 2;
        }
        else{ $status = 1;}
        return Subcategory::where('id', $id)->update([
            'status' => $status,
        ]);
    }
    public function updateSubcat(array $array, $id)
    {
        return Subcategory::where('id',$id)->update([
            'sub_name' => $array['name'],
            'category_id' => $array['category'],
            'slug' => $array['slug'],
        ]);
    }
    //
}
