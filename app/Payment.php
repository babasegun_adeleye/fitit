<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Payment extends Model
{
	use SoftDeletes;

	protected $fillable = ['user_id', 'order_id', 'amount', 'paid_at', 'type'];
	protected $dates = ['deleted_at'];

	public function add(array $array, $rid, $type)
	{
		$dt = new Carbon($array['data']['paidAt']);
		return Payment::create([
			'user_id' => auth()->user()->id,
			'order_id' => $rid,
			'amount' => sprintf('%.2f', $array['data']['amount'] / 100),
			'paid_at' =>  $dt->toDateTimeString(),
			'type' => $type,
		]);
	}
    //
}
