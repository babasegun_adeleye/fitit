<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	protected $fillable = [
        'country_name', 'country_code',
    ];

    public function cities()
    {
       return $this->hasMany(City::class)->orderBy('city_name', 'asc');
    }

     public function getAll()
     {
        return Country::latest();
     }

     public function add(array $data)
     {
        $ctry = Country::create([
            'country_name' => $data['name'],
            'country_code' => $data['code']
        ]);

        return $ctry;
     }

     public function firstByColumn($field, $param)
     {
        return Country::where($field, $param)->firstOrFail();
     }

     public function getByColumn($field, $param)
     {
        return Country::where($field, $param)->get();
     }

     public function remove($id)
     {
        $ctry = $this->firstByColumn('id', $id);
        if($ctry->status == 1)
        {
            $status = 2;
        }
        else{ $status = 1;}
        return Country::where('id', $id)->update([
            'status' => $status,
        ]);
     }

     public function updateCountry(array $data, $id)
     {
        return Country::where('id',$id)->update([
            'country_name' => $data['name'],
            'country_code' => $data['code'],
        ]);
     }
    //
}
