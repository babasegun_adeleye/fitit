<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
	protected $fillable = [
        'user_id', 'city_id', 'firstname', 'lastname', 'phone', 'address', 'img', 'description', 'store_name', 'storeurl',
    ];

	public function users()
	{
      return belongsTo(User::class);
  }

    public function products()
    {
      return $this->hasMany(Product::class);
    }

  	public function add(array $data, $id)
     {
        return Merchant::create([
            'user_id' => $id,
            'city_id' => $data['city'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'store_name' => $data['storename'],
            'storeurl'=> $data['surl'],
         	'description'=> $data['desc'],
         	'address'=> $data['address'],
        ]);
        
     }

     public function updateDetails(array $data, $id)
     {
        return Merchant::where('id',$id)->update([
            'store_name' => $data['name'],
            'storeurl' => $data['url'],
            'phone' => $data['phone'],
            'description'=> $data['desc'],
            'city_id' => $data['city'],
            'address'=> $data['address'],
        ]);
     }

     public function updateBanner($data, $id)
     {
        return Merchant::where('id', $id)->update([
          'img'=> $data,
        ]);
     }

     public function getData($field, $param)
	{
		return Merchant::where($field,$param)->first();
	}

    public function getOtherProducts($mid, $pid)
    {
        $count = Merchant::find($mid)->products()->whereNotIn('id', [$pid])->count();

        if($count > 2)
        {
            return Merchant::find($mid)->products()->whereNotIn('id', [$pid])->get()->random(3);
        }
        else{
            return Merchant::find($mid)->products()->whereNotIn('id', [$pid])->get()->random(0);
        }
        
    }

    public function storeExists($name)
    {
      return Merchant::where('storeurl', $name)->exists();
    }
    //
}
