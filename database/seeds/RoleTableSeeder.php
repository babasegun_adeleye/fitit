<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_customer = new Role();
    	$role_customer->name = 'customer';
    	$role_customer->description = 'A Customer User';
   	    $role_customer->save();

   		 $role_merchant = new Role();
   		 $role_merchant->name = 'merchant';
    	 $role_merchant->description = 'A Merchant User';
    	 $role_merchant->save();

    	 $role_merchant = new Role();
   		 $role_merchant->name = 'admin';
    	 $role_merchant->description = 'A Administrative User';
    	 $role_merchant->save();
        //
    }
}
