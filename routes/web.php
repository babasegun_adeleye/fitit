<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('checkout', 'CartController@checkout')->middleware('auth')->name('checkout.show');

Route::get('/', 'HomeController@index')->name('home');

//Route::get('mail', 'HomeController@mail')->name('mail');

Route::get('category/{slug}', 'HomeController@category')->name('category');
Route::get('category/{slug}/{sub}', 'HomeController@subcategory')->name('subcat');

Route::any('search', 'HomeController@search')->name('search');


Route::get('product/{slug}', 'HomeController@showProduct')->name('product.show');
Route::get('cart','HomeController@showCart')->name('cart.index');
//Route::get('checkout','HomeController@checkout')->middleware('auth')->name('checkout.show');


Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('logout','Auth\LoginController@logout')->name('logout');

Route::post('login', 'Auth\LoginController@login')->name('auth.login');

Route::get('register', 'RegistrationController@index')->name('register');
Route::get('sell', 'RegistrationController@shop')->name('register.shop');
Route::get('verify/email/{email}/token/{token}','RegistrationController@verify')->name('email.verify');

Route::post('register', 'RegistrationController@store')->name('register.store');
Route::post('sell', 'RegistrationController@sell')->name('sell');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

/*CartController*/
//Route::get('checkout','CartController@checkout')->middleware('auth')->name('checkout.show');
Route::get('/cart/remove','CartController@clear')->name('cart.empty');

Route::post('cart','CartController@store')->name('cart.store');
Route::post('cart/update/{id}','CartController@update')->name('cart.edit');
Route::post('checkout','CartController@order')->middleware('auth')->name('cart.order');


Route::delete('cart/remove/{id}','CartController@destroy')->name('cart.remove');

/*PaymentController*/
Route::get('pay', 'PaymentController@show')->middleware('auth')->name('pay.show');
Route::post('/pay', 'PaymentController@redirectToGateway')->middleware('auth')->name('pay');
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback')->middleware('auth');

/*MerchantController*/
Route::get('/{store}', 'MerchantController@store');

Route::get('/dashboard/{any}','AdminController@index')->where('any', '.*')->middleware('auth');






