<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->namespace('Api')->group(function() {

	/*CountryController*/
	Route::get('/countries', 'CountryController@index');
	Route::get('/country', 'CountryController@active');
	Route::get('/country/{id}', 'CountryController@show');

	Route::post('/country/add', 'CountryController@store');
	Route::put('/country/{id}', 'CountryController@update');

	Route::delete('/country/{id}', 'CountryController@destroy');

	/*CityController*/
	Route::get('/cities', 'CityController@index');
	Route::get('/cities/list', 'CityController@cities');
	Route::get('/city/{id}', 'CityController@show');

	Route::post('/city/add','CityController@store');
	Route::put('/city/{id}', 'CityController@update');

	Route::delete('/city/{id}','CityController@destroy');

	/*UserController*/
	Route::post('/password/change','UserController@changePassword');

	/*OrderController*/
	Route::get('/myOrders', 'OrderController@customerOrders');
	Route::get('/storeOrders', 'OrderController@merchantOrders');
	Route::get('/order/details/{num}', 'OrderController@details');

	Route::apiResources([
		'categories' => 'CategoryController',
		'subcategories' => 'SubcategoryController',
		'products' => 'ProductController',
		'billings' => 'BillingController'
	]);

	Route::post('/product/edit/{id}', 'ProductController@update');

	Route::get('/category/subcat/{id}', 'CategoryController@subCats');

	Route::get('/store/details', 'MerchantController@details');

	Route::get('/billing/{id}', 'BillingController@updateMain');

	Route::post('/store/banner', 'MerchantController@changeBanner');
	Route::post('/store/{id}', 'MerchantController@update');
});

Route::middleware(['ajax'])->group(function() {
	Route::get('/category/{id}/products', 'CategoryController@products');
	Route::get('/subcategory/{id}/products', 'CategoryController@subProducts');
	Route::get('/category/list/{id}', 'CategoryController@list');
	Route::get('/subcategory/list/{id}', 'CategoryController@subList');
	Route::get('/subcategory/category/{id}', 'CategoryController@category');
	Route::get('/checkout', 'CartController@checkout');
});