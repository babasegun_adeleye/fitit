@extends('layouts.master')

@section('body-class')
class=" shop_grid_page"
@endsection

@section('content')

 <div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">
          <div class="category-description std">
            <div class="slider-items-products">
              <div id="category-desc-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col1 owl-carousel owl-Template"> 
                  
                  <!-- Item -->
                  @if($data->img != null)
                  <div class="item"> <a href="#x"><img alt="{{$data->store_name}}" src="{{ asset('/images/banners').'/'.$data->img }}"></a> </div>
                  @endif
                  <!-- End Item --> 
                  
                </div>
              </div>
            </div>
          </div>
          <div class="shop-inner">
            <div class="page-title">
              <h2>{{ucwords($data->store_name)}}</h2>
            </div>
            
            <div class="product-grid-area">
              <ul class="products-grid">
              	@include('layouts.messages')
                @if(count($prods) > 0)
			    @foreach($prods as $prod) 
                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">
                  <div class="product-item">
                    <div class="item-inner">
                      <div class="product-thumbnail">
                        <div class="pr-img-area"> <a title="{{$prod->product_name}}" href="/product/{{$prod->id}}">
                          <figure> 
                            <img class="first-img" src="{{ asset('images/product/thumbnail').'/'.$prod->img }}" alt="{{$prod->product_name}}"> 
                          	@if(sizeof($prod->images) > 0) 
                            @foreach($prod->images as $im)  
                            <img class="hover-img" src="{{ asset('images/product/thumbnail').'/'.$im->img  }}" alt="{{$prod->product_name}}"> 
                            @break 
                            @endforeach 
                            @endif 
                          </figure>
                          </a> 
                        </div>

                        <div class="pr-info-area">
                          <div class="pr-button">
                            <!--<div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>-->
                          </div>
                        </div>
                      </div>
                      <div class="item-info">
                        <div class="info-inner">
                          <div class="item-title"> 
                            <a title="{{$prod->product_name}}" href="/product/{{$prod->id}}">{{$prod->product_name}} </a> 
                          </div>
                          <div class="item-content">
                            
                            <div class="item-price">
                              <div class="price-box">
                                <p class="special-price"> 
                                  <span class="price-label">Special Price</span> 
                                  <span class="price"> ₦{{number_format($prod->amount, 2)}} </span> 
                                </p>
                              </div>
                            </div>
                            <div class="pro-action">
                            	@if($prod->quantity > 0)
												<form action="{{ route('cart.store') }}" method="POST">
												 {{csrf_field()}}
												<input type="hidden" name="id" value="{{$prod->id}}">
												<input type="hidden" name="name" value="{{$prod->product_name}}">
												<input type="hidden" name="price" value="{{$prod->amount}}">
												<input type="hidden" name="merchant" value="{{$prod->merchant_id}}">
												<input type="hidden" name="quantity" value="1">
                              <button type="submit" class="add-to-cart"><span> Add to Cart</span> </button>
                              </form>
							@else <button type="button" class="add-to-cart" disabled=""> Add to Cart</button> @endif
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                @endforeach
                @endif
              </ul>
            </div>
            <div class="pagination-area">
              {{$prods->links()}}
            </div>
          </div>
        </div>
        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">
          <div class="block category-sidebar">
            <div class="sidebar-title">
              <h3>Categories</h3>
            </div>
            
          </div>
          <div class="block shop-by-side">
            <div class="sidebar-bar-title">
              <h3>Shop By</h3>
            </div>
            <div class="block-content">
              <p class="block-subtitle">Shopping Options</p>
              <div class="layered-Category">
                <h2 class="saider-bar-title">Categories</h2>
                <div class="layered-content">
                  <div id='cssmenu'>
                    <ul>
                      @foreach($thecat as $mycat)
                      <li class='has-sub'><a href="#"><span>{{$mycat->cat_name}}</span></a>
                        <ul>
                          @foreach($mycat->subcategory as $subs)
                          <li><a href=""><span>{{$subs->sub_name}}</span></a></li>
                          @endforeach
                        </ul>
                      </li>
                       @endforeach                    
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </aside>
      </div>
    </div>
  </div>
  @endsection

  @section('scripts')
  <script>
  	$(document).ready(function() {
	$(".pagination-area ul").removeClass("pagination");
	$(".pagination li a").addClass("page-link");

   $('#cssmenu > ul > li ul').each(function(index, e){
  var count = $(e).find('li').length;
  var content = '<span class=\"cnt\"> <i class=\"fa fa-plus\"></i></span>';
  $(e).closest('li').children('a').append(content);
});
$('#cssmenu ul ul li:odd').addClass('odd');
$('#cssmenu ul ul li:even').addClass('even');
$('#cssmenu > ul > li > a').click(function() {
  $('#cssmenu li').removeClass('active');
  $(this).closest('li').addClass('active'); 
  var checkElement = $(this).next();
  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
    $(this).closest('li').removeClass('active');
    checkElement.slideUp('normal');
  }
  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
    $('#cssmenu ul ul:visible').slideUp('normal');
    checkElement.slideDown('normal');
  }
  if($(this).closest('li').find('ul').children().length == 0) {
    return true;
  } else {
    return false; 
  }   
});

	});
 </script>
@endsection