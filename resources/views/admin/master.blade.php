
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>
  

  <link rel="stylesheet" href="/css/app.css">
  <script type='text/javascript'>
             window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light navbar-secondary border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

        <!-- Right navbar links -->
   
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name', 'Laravel') }}" class="brand-image img-square elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
       
        <div class="info">
          <h4  class="d-block" style="color: #fff;font-size: 18px;">Hello {{ucwords(auth()->user()->username)}}</h4>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
           @if(Auth::user()->can('isAdmin'))   
          <li class="nav-item">
            <router-link to="/dashboard/categories" class="nav-link" >
              <i class="nav-icon fas fa-th"></i>
              <p>
                Categories
              </p>
            </router-link>
          </li>

          <li class="nav-item">
            <router-link to="/dashboard/countries" class="nav-link" >
              <i class="nav-icon fas fa-flag"></i>
              <p>
                Countries
              </p>
            </router-link>
          </li>

          <li class="nav-item">
            <router-link to="/dashboard/cities" class="nav-link" >
              <i class="nav-icon fas fa-map-marker"></i>
              <p>
                Cities
              </p>
            </router-link>dashboard/orders
          </li>

          <li class="nav-item">
            <router-link to="/dashboard/subcategories" class="nav-link" >
              <i class="nav-icon fas fa-th"></i>
              <p>
                Subategories
              </p>
            </router-link>
          </li>
          @endif
           @if(Auth::user()->can('isMerchant'))
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="fas fa-shopping-cart nav-icon"></i>
              <p>
                Products
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <li class="nav-item">
                <router-link to="/dashboard/products" class="nav-link">
                  <i class="fas fa-eye nav-icon"></i>
                  <p>View Products</p>
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/dashboard/product/add" class="nav-link">
                  <i class="fas fa-plus nav-icon"></i>
                  <p>Add Product</p>
                </router-link>
              </li>
            </ul>
          </li>
           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="fas fa-shopping-basket  nav-icon"></i>
              <p>
                Store
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <li class="nav-item">
                <a href="/{{Auth::user()->merchant->storeurl}}" class="nav-link">
                  <i class="fas fa-eye nav-icon"></i>
                  <p>My Store</p>
                </a>
              </li>
              <li class="nav-item">
                <router-link to="/dashboard/store" class="nav-link">
                  <i class="fas fa-cogs nav-icon"></i>
                  <p>Store Settings</p>
                </router-link>
              </li>
            </ul>
          </li>
          @endif
          <li class="nav-item">
            <router-link to="/dashboard/orders" class="nav-link" >
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Orders
              </p>
            </router-link>
          </li>


            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="fas fa-cogs nav-icon"></i>
              <p>
                Settings
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <!--  <li class="nav-item">
                <router-link to="/dashboard/products" class="nav-link">
                  <i class="fas fa-cog nav-icon"></i>
                  <p>Account Settings</p>
                </router-link>
              </li> -->
              <li class="nav-item">
                <router-link to="/dashboard/password/change" class="nav-link">
                  <i class="fas fa-key nav-icon"></i>
                  <p>Change Password</p>
                </router-link>
              </li>
              @if(Auth::user()->can('isCustomer') || Auth::user()->can('isAdmin'))
              <li class="nav-item">
                <router-link to="/dashboard/address" class="nav-link">
                  <i class="fas fa-map-marker nav-icon"></i>
                  <p>Addresses</p>
                </router-link>
              </li>
              @endif
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{route('logout')}}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
               Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <br> <br>

    <!-- Main content -->
    <div class="content" >
      <div class="container-fluid">
      @yield('content')
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{now()->year}} <a href="http://fititng.com">FititNg</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

@auth
<script>
  window.user = '{{$user}}'
</script>
@endauth
<script src="https://cdn.rawgit.com/cretueusebiu/412715093d6e8980e7b176e9de663d97/raw/aea128d8d15d5f9f2d87892fb7d18b5f6953e952/objectToFormData.js"></script>
<script src="/js/app.js"></script>


@hasSection('script')
		@yield('script')
		@endif
    
</body>
</html>
