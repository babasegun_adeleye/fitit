
<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>{{ config('app.name', 'Laravel') }}</title>	

		<meta name="keywords" content="Fitit" />
		<meta name="description" content="Online Shopping ">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" >
		<link href='//fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'/>
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

		<!-- Vendor CSS -->
		
		<link rel="stylesheet"  href="/css/bootstrap.min.css" />
		<link rel="stylesheet"  href="/css/bootstrap-grid.min.css" />
		<link rel="stylesheet"  href="/css/animate.css" />
		<link rel="stylesheet"  href="/css/font-awesome.min.css" />
		<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.0/css/simple-line-icons.css"/>
		<link rel="stylesheet"  href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css"/>
		<link rel="stylesheet"  href="/css/owl.carousel.css"/>
		<link rel="stylesheet"  href="/css/owl.transitions.css"/>
		<link rel="stylesheet"  href="/css/flexslider.css"/>
		<link rel="stylesheet"  href="/css/jquery-ui.css"/>
		<link rel="stylesheet"  href="/css/revolution-slider.css"/>
		<link rel="stylesheet"  href="/css/quick_view_popup.css"/>
		<link rel="stylesheet"  href="/css/shortcode.css"/>
		<link rel="stylesheet"  href="/css/shortcodes/shortcodes.css"/>
		<link rel="stylesheet"  href="/css/shortcodes/featured-box.css"/>
		<link rel="stylesheet"  href="/css/shortcodes/pricing-table.css"/>
		<link rel="stylesheet"  href="/css/shortcodes/tooltip.css"/>
		<link rel="stylesheet"  href="/css/shortcodes/post.css"/>
		<link rel="stylesheet"  href="/css/style.css"/>
		<link rel="stylesheet"  href="/css/responsive.css"/>
		<link rel="stylesheet"  href="/css/custom.css"/>

		@hasSection('css')
		@yield('css')
		@endif
		
		
			
		

	</head>
	<body @hasSection('body-class') @yield('body-class') @endif>

		@include('layouts.mobile')
		<div id="page"> 
		@include('layouts.header')
		@include('layouts.navbar')

		@yield('content')

		@include('layouts.footer')
	   </div>

		<!-- Vendor -->

		<script  src="/js/jquery.min.js"></script> 
		
		<script  src="/js/bootstrap.min.js"></script> 
		<script  src="/js/owl.carousel.min.js"></script> 

		<!-- bxslider js --> 
		<script  src="/js/jquery.bxslider.js"></script> 

		<!-- jquery.mobile-menu js --> 
		<script  src="/js/mobile-menu.js"></script> 

		<!--jquery-ui.min js --> 
		<script  src="/js/jquery-ui.js"></script> 

		<!-- main js --> 
		<script  src="/js/main.js"></script>

		<script  src="/js/countdown.js"></script> 

		<!-- Slider Js --> 
		<script  src="/js/revolution-slider.js"></script> 


		<!-- Current Page Vendor and Views -->
		@hasSection('scripts')
		@yield('scripts')
		@endif
	
		<script type="text/javascript">
			$(document).ready(function(){

			@if(Cart::count() > 0)
			@foreach(Cart::content() as $item)
				var del = document.getElementById('cart-del{{$item->id}}');
			$('#btn-cart{{$item->id}}').click(function()
			{
				del.submit();
			});
			@endforeach
			@endif
		});

		</script>
		
		@hasSection('script')
		@yield('script')
		@endif




	</body>
</html>
