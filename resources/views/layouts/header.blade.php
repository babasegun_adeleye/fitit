

          <header>
    <div class="header-container">
      <div class="header-top">
        <div class="container">
          <div class="row">
            <div class="col-sm-4 col-md-4 col-xs-12"> 
              <!-- Default Welcome Message -->
              @guest
              <div class="welcome-msg hidden-xs hidden-sm">Welcome!</div>
              @else
               @if(!empty(auth()->user()->profile))
              <div class="welcome-msg hidden-xs hidden-sm">Welcome {{auth()->user()->profile->firstname}} {{auth()->user()->profile->lastname}}! </div>
              @endif
              @if(!empty(auth()->user()->merchant))
              <div class="welcome-msg hidden-xs hidden-sm">Welcome to {{auth()->user()->merchant->store_name}}! </div>
              @endif
              @endguest
              <!-- Language &amp; Currency wrapper -->
            </div>
            
            <!-- top links -->
            <div class="headerlinkmenu col-md-8 col-sm-8 col-xs-12"> 
              <span class="phone  hidden-xs hidden-sm">Call Us: +234 123 456 7890</span>
              <ul class="links">
              	@guest
                <li><a title="Create an Account" href="{{route('register')}}"><span>Create an Account</span></a></li>
                <li><a title="Sell" href="{{route('register.shop')}}"><span>Sell</span></a></li>
                <li><a title="login" href="{{route('login')}}"><span>Login</span></a></li>
                @else
                <li>
                  <div class="dropdown">
                    @if(!empty(auth()->user()->profile))
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>My Account</span> <i class="fa fa-angle-down"></i></a>
                     <ul class="dropdown-menu" role="menu">
                     <!--  <li><a href="">Profile</a></li> -->
                      <li><a href="/dashboard/orders">Orders</a></li>
                      <li class="divider"></li>
                      <li><a href="{{route('logout')}}">Log Out</a></li>
                    </ul>
                    @else
                     <a class="current-open" href=""><span>Dashboard</span></a>
                    @endif
                  </div>
                </li>
                <li><a title="Logout" href="{{route('logout')}}"><span>Log Out</span></a></li>
                @endguest
              </ul>
            </div>


          </div>
        </div>
      </div>

      @include('layouts.messages')



      
      <!-- header inner -->
      <div class="header-inner">
        <div class="container">
          <div class="row">
            <div class="col-sm-3 col-xs-12 jtv-logo-block"> 
              
              <!-- Header Logo -->
              <div class="logo"><a title="e-commerce" href="/"><img alt="Fitit" title="Fitit" src="{{ asset('images/logo.png') }}"></a> </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-6 jtv-top-search"> 
              
              <!-- Search -->
              
              <div class="top-search">
                <div id="search">
                  <form method="POST" action="{{route('search')}}">
                    @csrf
                    <div class="input-group">
                      <select class="cate-dropdown hidden-xs hidden-sm" name="category">
                        <option value="all">All Categories</option>
                        @foreach($cats as $cat)
                        <option value="{{$cat->slug}}">{{$cat->cat_name}}</option>
                        @endforeach
                        <!--<option>&nbsp;&nbsp;&nbsp;Chair </option>-->
                      </select>
                      <input type="text" class="form-control" placeholder="Enter your search..." name="search">
                      <button class="btn-search" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                  </form>
                </div>
              </div>
              
              <!-- End Search --> 
              
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 top-cart">
              <!--<div class="link-wishlist"> <a href="#"> <i class="icon-heart icons"></i><span> Wishlist</span></a> </div> --> 
              <!-- top cart -->
              <div class="top-cart-contain">
                <div class="mini-cart">
                  <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#">
                    <div class="cart-icon"><i class="icon-basket-loaded icons"></i><span class="cart-total">{{Cart::count()}}</span></div>
                    <div class="shoppingcart-inner hidden-xs"><span class="cart-title">My Cart</span> </div>
                    </a></div>
                  <div>
                  	@if(Cart::count() > 0)
                  	<?php $num = 0; ?>
                    <div class="top-cart-content">
                      <div class="block-subtitle hidden">Recently added items</div>
                      <ul id="cart-sidebar" class="mini-products-list">
                      	@foreach(Cart::content() as $item)
                      	@if($num < 3)
                        <li class="item odd"> <a href="{{route('product.show',$item->id)}}" title="{{$item->name}}" class="product-image"><img src="{{ asset('images/product/thumbnail').'/'.$item->model->img }}" alt="{{$item->name}}" width="65"></a>
                          <div class="product-details">
                           <form  action="{{route('cart.remove',$item->rowId)}}" method="POST" id="cart-del{{$item->id}}">
                          	@csrf
                          	<input name="_method" type="hidden" value="DELETE">
                          	<a href="#" title="Delete {{$item->name}}" class="remove-cart" id="btn-cart{{$item->id}}"><i class="pe-7s-trash"></i></a>
                          </form>
                            <p class="product-name"><a href="{{route('product.show',$item->id)}}">{{$item->name}}</a> </p>
                            <strong>{{$item->qty}}</strong> x <span class="price">₦{{number_format($item->price,2)}}</span> </div>
                        </li>
                        @else break; @endif
                       @endforeach
                      </ul>
                      <div class="top-subtotal">Subtotal: <span class="price">₦ {{Cart::subtotal()}}</span></div>
                      <div class="actions">
                        <button class="btn-checkout" type="button" onClick="location.href='{{route('checkout.show')}}'" ><i class="fa fa-check"></i><span>Checkout</span></button>
                        <button class="view-cart" type="button" onClick="location.href='{{route('cart.index')}}'"><i class="fa fa-shopping-cart"></i><span>View Cart</span></button>
                      </div>
                    </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      
    </div>
  </header>











































