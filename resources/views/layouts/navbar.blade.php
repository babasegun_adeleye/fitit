
<nav>
    <div class="container">
      <div class="row">
        <div class="mm-toggle-wrap">
          <div class="mm-toggle"><i class="fa fa-align-justify"></i> </div>
          <span class="mm-label">All Categories</span> </div>
        <div class="col-md-3 col-sm-3 mega-container hidden-xs">
          <div class="navleft-container">
            <div class="mega-menu-title">
              <h3><span>All Categories</span></h3>
            </div>
            
            <!-- Shop by category -->
            <div class="mega-menu-category">
              <ul class="nav">
                @foreach($cats as $cat)
                <li ><a href="{{route('category', $cat->slug)}}">{{$cat->cat_name}}</a>
                 <div class="wrap-popup column1">
                    <div class="popup">
                      <ul class="nav">
                        @foreach($cat->subcategory as $subcat)
                        <li><a href="/category/{{$cat->slug}}/{{$subcat->slug}}"><span>{{$subcat->sub_name}}</span></a></li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-9 col-sm-9 jtv-megamenu">
          <div class="mtmegamenu">
            <ul class="hidden-xs">
              <li class="mt-root demo_custom_link_cms">
                <div class="mt-root-item"><a href="/">
                  <div class="title title_font"><span class="title-text">Home</span></div>
                  </a></div>
              </li>
              <li class="mt-root">
                <div class="mt-root-item"><a href="#">
                  <div class="title title_font"><span class="title-text">Categories</span></div>
                  </a></div>
                <ul class="menu-items col-xs-12">
                   @foreach($cats as $cat)
                   <?php $num = 0;?>
                  <li class="menu-item depth-1 menucol-1-3 ">
                    <div class="title title_font"> <a href="{{route('category', $cat->slug)}}">{{$cat->cat_name}}</a></div>
                    <ul class="submenu">
                      @foreach($cat->subcategory as $subcat)
                      @if($num < 5)
                      <li class="menu-item">
                        <div class="title"> <a href="/category/{{$cat->slug}}/{{$subcat->slug}}">{{$subcat->sub_name}}</a></div>
                      </li>
                      <?php $num = $num+1; ?>
                      @endif
                      @endforeach
                    </ul>
                  </li>
                  @endforeach
                </ul>
              </li>
            
   
              <li></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </nav>