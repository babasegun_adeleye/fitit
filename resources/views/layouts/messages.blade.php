@if ($errors->any())
  <div class="alert alert-danger">
  	 <a href="#" class="close" data-dismiss="alert">&times;</a>
	 @foreach ($errors->all() as $error)
	 <strong>Error!</strong> {{ $error }}
	 <br>
	  @endforeach
	  </div>
	@endif

	@if (session()->has('message'))
  <div class="alert alert-success text-center">
  	 <a href="#" class="close" data-dismiss="alert">&times;</a>
  	  <strong>Success!</strong> {{ session()->get('message')}}
	  </div>
	@endif

	@if (session()->has('status'))
  <div class="alert alert-success text-center">
  	 <a href="#" class="close" data-dismiss="alert">&times;</a>
  	  <strong>Success!</strong> {{ session()->get('status')}}
	  </div>
	@endif

	@if (session()->has('warning'))
  <div class="col-md-6 col-md-offset-5 alert alert-warning text-center">
  	 <a href="#" class="close" data-dismiss="alert">&times;</a>
  	  <strong>Warning!</strong> {{ session()->get('warning')}}
	  </div>
	@endif

	

