@extends('layouts.master')

@section('body-class')
class=" product-page"
@endsection

@section('css')

@endsection

@section('content')


<div class="main-container col1-layout">
    <div class="container">
      <div class="row">
        <div class="col-main">
          <div class="product-view-area">
            <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
              
             
              <div class="large-image"> <a href="{{ asset('images/product').'/'.$data->img }}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="zoom-img" src="{{ asset('images/product').'/'.$data->img }}" alt="{$data->product_name}}"> </a> </div>
            
              
              <!-- end: more-images --> 
              
            </div>
            <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">
              <div class="product-name">
                <h1>{{$data->product_name}}</h1>
              </div>
              <div class="price-box">
                <p class="special-price"> <span class="price-label">Price</span> <span class="price"> ₦ {{$data->amount}} </span> </p>
              </div>
              <div class="ratings">
                
                <p class="rating-links"> <a href="" style="font-size: 14px; font-weight: bolder;">{{$data->merchant->store_name}} </a></p>
                <p class="availability in-stock pull-right">Availability: @if($data->quantity > 0)<span>In Stock</span>@else<span style="background: #e83f33;">Out Of Stock</span>@endif</p>
              </div>
              <div class="short-description">
                <h2>Quick Overview</h2>
                <p>{{str_before($data->product_desc,'.')}}.</p>
               
              </div>
             
              <div class="product-variation">
              	@include('layouts.messages')
                <form  action="{{ route('cart.store') }}" method="POST" class="cart">
                	@csrf
                  <div class="cart-plus-minus">
                    <label for="qty">Quantity:</label>
                    <div class="numbers-row">
                    	<input type="hidden" name="id" value="{{$data->id}}">
					    <input type="hidden" name="name" value="{{$data->product_name}}">
						<input type="hidden" name="price" value="{{$data->amount}}">
						<input type="hidden" name="merchant" value="{{$data->merchant_id}}">
						<input type="hidden" name="quantity" id="quantity" value="1">
                    	<?php if($data->quantity > 0): $min = 1; else: $min = 0; endif?>
                    	<?php $max = $data->quantity; ?>
                      <div class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
                      <input type="text"   class="qty" title="Qty" value="{{$min}}" maxlength="12" id="qty" disabled >
                      <div  class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
                    </div>
                  </div>
                  @if($data->quantity > 0)
                  <button class="button pro-add-to-cart" title="Add to Cart" type="submit"><span><i class="fa fa-shopping-basket"></i> Add to Cart</span></button>
                  @else
                  <button class="button pro-add-to-cart" title="Add to Cart" type="button" disabled><span><i class="fa fa-shopping-basket"></i> Add to Cart</span></button>
                  @endif
                </form>
              </div>
            
          </div>
        </div>
        <div class="product-overview-tab">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="product-tab-inner">
                  <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                    <li class="active"> <a href="#description" data-toggle="tab"> Description </a> </li>
                    <!-- <li> <a href="#custom_tabs" data-toggle="tab">Custom Tab</a> </li> -->
                  </ul>
                  <div id="productTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="description">
                      <div class="std">
                        <p>{{$data->product_desc}}</p>
                      </div>
                    </div>
                   
                   
                   <!--  <div class="tab-pane fade" id="custom_tabs">
                      <div class="product-tabs-content-inner clearfix">
                        <p></p>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
@if(count($prod) > 0)
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="related-product-area">
          <div class="page-header">
            <h2>Products From Merchant</h2>
          </div>
          <div class="related-products-pro">
            <div class="slider-items-products">
              <div id="related-product-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col4 fadeInUp">
                	@foreach($prod as $prods)
                  <div class="product-item">
                    <div class="item-inner">
                      <div class="product-thumbnail">
                        <div class="pr-img-area"> <a title="Product title here" href="/product/{{$prods->id}}">
                          <figure> <img class="first-img" src="{{ asset('images/prodct/thumbnail').'/'.$prods->img }}" alt="HTML template"> 
                          	<!-- <img class="hover-img" src="images/products/product-6.jpg" alt="HTML template"> -->
                          </figure>
                          </a> </div>
                        <div class="pr-info-area">
                          <div class="pr-button">
                            <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart-o"></i> </a> </div>
                            <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-link"></i> </a> </div>
                            <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                          </div>
                        </div>
                      </div>
                      <div class="item-info">
                        <div class="info-inner">
                          <div class="item-title"> <a title="{{$prods->product_name}}" href="/product/{{$prods->id}}">{{$prods->product_name}} </a> </div>
                          <div class="item-content">
                            <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                            <div class="item-price">
                              <div class="price-box"> <span class="regular-price"> <span class="price">₦ {{$prods->amount}}</span> </span> </div>
                            </div>
                            <div class="pro-action">
                              <button type="button" class="add-to-cart"><span> Add to Cart</span> </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
            @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 @endif

@endsection
				
@section('scripts')
<script type="text/javascript" src="{{ asset('js/cloud-zoom.js') }}"></script> 
<script type="text/javascript" src="{{ asset('js/jquery.flexslider.js') }}"></script> 

@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function(){

		
		var data = document.getElementById('qty');
		var qid = document.getElementById('quantity');

		$('.dec').click(function()
		{
			var val = data.value;
			var low = <?php echo $min ?>;
			if(val > low)
			{
				data.value = val - 1;
				qid.value = data.value;
			}
			else{ alert("Quantity cannot be below "+low)}

		});

		$('.inc').click(function()
		{
			var val = data.value;
			var high = <?php echo $max ?>;
			if(val < high)
			{
				data.value = parseInt(val)+1;
				qid.value = data.value;
			}
			else{ alert("Quantity cannot be more than "+high)}
		});

	
	});
</script>
@endsection