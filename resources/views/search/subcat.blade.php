@extends('layouts.master')

@section('scripts')
	<script src="/js/custom.js"></script>
@endsection

@section('content')
<div id="page">
<subcategory-component sid="{{$id}}" tag="{{$name}}"></subcategory-component>
</div>
@endsection
