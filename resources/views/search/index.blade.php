@extends('layouts.master')

@section('content')
<br><br><br>
<div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        <div class="col-main col-sm-12 col-xs-12">
         
          <div class="shop-inner">
            <div class="page-title">
              <h2>You searched in {{$q}}</h2>
            </div>
            @isset($search)
            <div class="product-grid-area">
              @if(count($search) > 0)
              <ul class="products-grid">
                @foreach($search as $prod)
                <li class="item col-lg-3 col-md-3 col-sm-6 col-xs-6 ">
                  <div class="product-item">
                    <div class="item-inner">
                      <div class="product-thumbnail">
                        <div class="pr-img-area"> <a title="{{$prod->product_name}}" href="single_product.html">
                          <figure> <img class="first-img" src="{{ asset('images/product').'/'.$prod->img }}" alt="HTML template"> <img class="hover-img" src="images/products/product-1.jpg" alt="HTML template"></figure>
                          </a> </div>
                       
                      </div>
                      <div class="item-info">
                        <div class="info-inner">
                          <div class="item-title"> <a title="{{$prod->product_name}}" href="single_product.html">{{$prod->product_name}} </a> </div>
                          <div class="item-content">
                          
                            <div class="item-price">
                              <div class="price-box"> <span class="regular-price"> <span class="price">₦{{number_format($prod->amount)}}</span> </span> </div>
                            </div>
                            <div class="pro-action">
                                @if($prod->quantity > 0)
                                <form action="{{ route('cart.store') }}" method="POST">
                               @csrf
                              <input type="hidden" name="id" value="{{$prod->id}}">
                              <input type="hidden" name="name" value="{{$prod->product_name}}">
                              <input type="hidden" name="price" value="{{$prod->amount}}">
                              <input type="hidden" name="merchant" value="{{$prod->merchant_id}}">
                              <input type="hidden" name="quantity" value="1">
                              <button type="submit" class="add-to-cart"><span> Add To Cart</span> </button>
                            </form>
                            @else <button type="button" class="add-to-cart" disabled=""> Add To Cart</button> @endif
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                @endforeach
              </ul>
              @else
               <h3 class="text-center mt-7">No Data available</h3>
              @endif
            </div>
            
            @endisset
           
            <div class="pagination-area">
              @isset($search)
              {{$search->links()}}
              @endisset
            </div>
          </div>
        </div>
       
      </div>
    </div>
  </div>
@endsection

@section('scripts')
        <script>
          $(document).ready(function() {
            $(".pagination-area ul").removeClass("pagination");
          });
        </script>
        @endsection