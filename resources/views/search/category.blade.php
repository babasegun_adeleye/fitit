@extends('layouts.master')

@section('scripts')
	<script src="/js/custom.js"></script>
@endsection

@section('content')
<div id="page">
<home-component cid="{{$id}}" tag="{{$name}}"></home-component>
</div>
@endsection
