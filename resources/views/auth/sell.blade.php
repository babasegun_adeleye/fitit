@extends('layouts.master')

@section('content')

				
<div class="container">

	<div class="row">
		<div class="col">
			<hr class="tall mb-4">
		</div>
	</div>

	<div class="row">
		<div class="col">
		<div class="featured-boxes">
		<div class="row">
					
		<div class="col-md-12">
			<div class="featured-box featured-box-primary text-left mt-3 mt-md-5">
				<div class="box-content register-border mb-7">
					<h3 class="heading-primary text-uppercase mb-3 text-center">Register Your Business</h3>
					@include('layouts.messages')
					 
				 <form action="/sell"  method="POST" style="    padding: 23px 20px 29px;">
					@csrf
					<div class="form-row">
							<div class="form-group col-lg-6 @if ($errors->has('username')) has-error @endif">
								<label>Username<em class="c-red">*</em></label>
								<input name="username" type="text" class="form-control form-control-lg" value="{{old('username')}}" required>
								 @if ($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
							</div>
								<div class="form-group col-lg-6 @if ($errors->has('email')) has-error @endif">
								<label>E-mail Address<em class="c-red">*</em></label>
								<input name="email" type="email"  class="form-control form-control-lg border-grey" value="{{old('email')}}" required>
								 @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
							</div>
							
						</div>
						 <div class="form-row">
							<div class="form-group col-lg-6 @if ($errors->has('storename')) has-error @endif">
								<label>Store Name<em class="c-red">*</em></label>
								<input name="storename" type="text" class="form-control form-control-lg border-grey" value="{{old('storename')}}" required>
								 @if ($errors->has('storename')) <p class="help-block">{{ $errors->first('storename') }}</p> @endif
							</div>
							<div class="form-group col-lg-6 @if ($errors->has('surl')) has-error @endif">
								<label>Store URL<em class="c-red">*</em></label>
								<div class="input-group mb-3">
								<span class="input-group-addon">fitit.com/</span>
								<input name="surl" type="text"  class="form-control form-control-lg border-grey" value="{{old('surl')}}" required>
								 @if ($errors->has('surl')) <p class="help-block">{{ $errors->first('surl') }}</p> @endif
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-lg-12 @if ($errors->has('desc')) has-error @endif">
								<label>Store Description<em class="c-red">*</em></label>
								<textarea name="desc" class="form-control form-control-lg border-grey" value="{{old('desc')}}" required></textarea>
								 @if ($errors->has('desc')) <p class="help-block">{{ $errors->first('desc') }}</p> @endif
							 </div>
						</div>
						<div class="form-row">
								<div class="form-group col-lg-6 @if ($errors->has('address')) has-error @endif">
								<label>Store Address<em class="c-red">*</em></label>
								<input name="address" type="text"  class="form-control form-control-lg border-grey" value="{{old('address')}}" required>
								 @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
							    </div>
							
								<div class="form-group col-lg-6 @if ($errors->has('city')) has-error @endif">
								<label>Store City<em class="c-red">*</em></label>
								<select name="city" class="form-control form-control-lg border-grey" required>
									@foreach($data as $cty)
								<option value="{{$cty->id}}" >{{$cty->city_name}}</option>
								@endforeach
								</select>
								 @if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
								</div>
						</div>
					 <div class="form-row">
							<div class="form-group col-lg-6 @if ($errors->has('firstname')) has-error @endif">
								<label>First Name<em class="c-red">*</em></label>
								<input name="firstname" type="text" class="form-control form-control-lg border-grey" value="{{old('firstname')}}" required>
								 @if ($errors->has('firstname')) <p class="help-block">{{ $errors->first('firstname') }}</p> @endif
							</div>
							<div class="form-group col-lg-6 @if ($errors->has('lastname')) has-error @endif">
								<label>Last Name<em class="c-red">*</em></label>
								<input name="lastname" type="text"  class="form-control form-control-lg border-grey" value="{{old('lastname')}}" required>
								 @if ($errors->has('lastname')) <p class="help-block">{{ $errors->first('lastname') }}</p> @endif
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-lg-12 @if ($errors->has('phone')) has-error @endif">
								<label>Phone Number<em class="c-red">*</em></label>
								<input name="phone" type="text" class="form-control form-control-lg border-grey" value="{{old('phone')}}" required>
								 @if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p> @endif
							</div>
						</div>
						<div class="form-row">
							
					
						</div>
						
						<div class="form-row">
							<div class="form-group col-lg-6 @if ($errors->has('password')) has-error @endif">
								<label>Password<em class="c-red">*</em></label>
								<input name="password" type="password" value="" class="form-control form-control-lg border-grey">
								 @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
							</div>
							<div class="form-group col-lg-6 @if ($errors->has('password_confirmation')) has-error @endif">
								<label>Re-enter Password<em class="c-red">*</em></label>
								<input name="password_confirmation" type="password" value="" class="form-control form-control-lg border-grey">
								 @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p> @endif
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<input type="submit" value="Sell" class="btn bg-red btn-md float-right mb-5 mgr-15 no-radius" data-loading-text="Loading...">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	</div>
	@endsection
				
	@section('scripts')
	
	@endsection