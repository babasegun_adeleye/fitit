@extends('layouts.master')

@section('content')

				
<div class="container">

	<div class="row">
		<div class="col">
			<hr class="tall mb-4">
		</div>
	</div>

	<div class="row">
		<div class="col">
		<div class="featured-boxes">
		<div class="row">
					
		<div class="col-md-12">
			<div class="featured-box featured-box-primary text-left mt-3 mt-md-5">
				<div class="box-content register-border mb-7">
					<h3 class="heading-primary text-uppercase mb-3 text-center">Login</h3>
					@include('layouts.messages')
					 
				 <form action="/login"  method="POST" style="    padding: 23px 20px 29px;">
					@csrf
				
						<div class="form-row">
							<div class="form-group col-lg-12 @if ($errors->has('email')) has-error @endif">
								<label>E-mail Address<em class="c-red">*</em></label>
								<input name="email" type="email"  class="form-control form-control-lg border-grey" value="{{old('email')}}" required>
								 @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
							</div>
						
						</div>
					
						
						<div class="form-row">
							<div class="form-group col-lg-12 @if ($errors->has('password')) has-error @endif">
								<label>Password<em class="c-red">*</em></label>
								<input name="password" type="password" value="" class="form-control form-control-lg border-grey">
								 @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
							</div>
						
						</div>
						<div class="form-row">
							<div class="form-group col">
								<a href="{{ route('password.request') }}" class="f-left">Forgot Your Password?</a>
								<input type="submit" value="Login" class="btn bg-red btn-md float-right mb-5 mgr-15 no-radius" data-loading-text="Loading...">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	</div>
	@endsection
				
	@section('scripts')
	
	@endsection