@extends('layouts.master')

@section('content')

				
<div class="container">

					<div class="row">
						<div class="col">
							<hr class="tall mb-4">
						</div>
					</div>

					<div class="row">
						<div class="col">
							<div class="featured-boxes">
								<div class="row">
									<div class="col-md-3">
										
									</div>
					<div class="col-md-6">
						<div class="featured-box featured-box-primary text-left mt-3 mt-md-5">
					<div class="box-content">
						<h3 class="heading-primary text-uppercase mb-3 text-center">Recover Password</h3>
						<p style="font-size: 14px;">Please enter your email address below. You will receive a link to reset your password.</p>
						@include('layouts.messages')
						<form action="{{route('password.email')}}" id="frmSignUp" method="POST">
							@csrf
						 <div class="form-row">
								<div class="form-group col-lg-12">
									<label>Email<em class="c-red">*</em></label>
									<input name="email" type="email" class="form-control form-control-lg border-grey" required>
								</div>
								
							</div>
							
							<div class="form-row">
								<div class="form-group col">
									<input type="submit" value="Submit" class="btn bg-red btn-md float-right mb-5 no-radius" data-loading-text="Loading...">
								</div>
							</div>
						</form>
					</div>
										</div>
									</div>
									<div class="col-md-3">
										
									</div>
								</div>

							</div>
						</div>
					</div>

				</div>
				@endsection
				
				