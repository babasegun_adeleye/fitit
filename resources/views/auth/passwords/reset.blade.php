@extends('layouts.master')

@section('content')
<div class="container">

    <div class="row">
        <div class="col">
            <hr class="tall mb-4">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="featured-boxes">
                <div class="row">
                    
                    <div class="col-md-12">
                        <div class="featured-box featured-box-primary text-left mt-3 mt-md-5">
                            <div class="box-content">
                                <h4 class="heading-primary text-uppercase mb-3">{{ __('Reset Password') }}</h4>

                                @include('layouts.messages')
                                <form action="{{ route('password.update') }}"  method="POST">
                                    @csrf

                                    <input type="hidden" name="token" value="{{ $token }}">

                                    <div class="form-row{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <div class="form-group col-lg-12">
                                            <label>{{ __('E-Mail Address') }}<em class="c-red">*</em></label>
                                             <input id="email" type="email" class="form-control @error('email') is-invalid @enderror border-grey" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        
                                    </div>

                                 <div class="form-row{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <div class="form-group col-lg-12">
                                            <label>{{ __('Password') }}</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror border-grey" name="password" required autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                             </span>
                                            @enderror
                                        </div>
                                        
                                    </div>
                                    <div class="form-row{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <div class="form-group col-lg-12">
                                            <label>{{ __('Confirm Password') }}</label>
                                            <input id="password-confirm" type="password" class="form-control border-grey" name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                        
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col">
                                            <button type="submit" class="btn btn-primary float-right mb-5">
                                    {{ __('Reset Password') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

                </div>
@endsection


