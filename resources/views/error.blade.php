@extends('layouts.master')
@section('content')
<div role="main" class="main">

				<section class="page-header">
					<div class="container">
						
						<div class="row">
							<div class="col">
								<h1>@yield('type')</h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<section class="page-not-found">
						<div class="row justify-content-center">
							<div class="col-lg-7 text-center">
								<div class="page-not-found-main">
									<h2>@yield('code') <i class="fa fa-file"></i></h2>
									<p>@yield('message')</p>
								</div>
							</div>
						</div>
					</section>

				</div>

			</div>
@endsection