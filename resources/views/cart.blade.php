@extends('layouts.master')

@section('body-class')
class="shopping_cart_page"
@endsection

@section('content')



 <section class="main-container col1-layout">
    <div class="main container">
      <div class="col-main">
        <div class="cart">
          
          <div class="page-content page-order"><div class="page-title">
            <h2>Shopping Cart</h2>
          </div>
           @include('layouts.messages')
            <?php $min=0; $max=0; ?>
            @if(Cart::count() > 0)
            <div class="order-detail-content">
              <div class="table-responsive">
                <table class="table table-bordered cart_summary">
                  <thead>
                    <tr>
                      <th class="cart_product"></th>
                      <th>Product</th>
                      <th>Unit price</th>
                      <th>Qty</th>
                      <th ></th>
                      <th>Total</th>
                      <th  class="action"><i class="fa fa-trash-o"></i></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach(Cart::content() as $item)
                    <tr>
                      <td class="cart_product"><a href="{{route('product.show',$item->model->productsku)}}"><img src="{{ asset('images/product').'/'.$item->model->img }}" alt="Product"></a></td>
                      <td class="cart_description"><p class="product-name"><a href="{{route('product.show',$item->model->productsku)}}">{{ $item->name}} </a></p></td>
                      <td class="price"><span>₦ {{ $item->price}}</span></td>
                      <form action="{{route('cart.edit',$item->rowId)}}" method="POST">
                      	@csrf
                      	<input type="hidden" name="quantity" id="quantity{{$item->id}}" value="1">
                      <?php $count = $item->model->quantity; ?>
                      <?php if($count > 0): $min = 1; else: $min = 0; endif;
                       $max = $item->model->quantity; ?>
                      <td class="qty"><div class="dec qtybutton" id="dec{{$item->id}}"><i class="fa fa-minus">&nbsp;</i></div><input class="form-control input-sm" type="text" value="{{ $item->qty }}" id="qty{{$item->id}}" disabled><div  class="inc qtybutton" id="inc{{$item->id}}"><i class="fa fa-plus">&nbsp;</i></div></td>
                      <td class="action"><button type="submit"  class="btn btn-default"><i class="fa fa-pencil"></i></button></td>
                    </form>
                      <td class="price"><span>₦ {{ $item->price*$item->qty }}</span></td>
                      <td class="action">
                      	<form  action="{{route('cart.remove',$item->rowId)}}" method="POST">
          						  @csrf
          						<input name="_method" type="hidden" value="DELETE">
                                	<button type="submit" class="btn btn-default spinner-down">
          						<i class="fa fa-times"></i>
          						</button>
          					</form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2" rowspan="2"></td>
                      <td colspan="3">	Total products (vat incl.)</td>
                      <td colspan="2">₦ {{ Cart::subtotal() }} </td>
                    </tr>
                    <tr>
                      <td colspan="3"><strong>Total</strong></td>
                      <td colspan="2"><strong>₦ {{ Cart::subtotal() }} </strong></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <div class="cart_navigation"> <a class="continue-btn" href="{{route('home')}}"><i class="fa fa-arrow-left"> </i>&nbsp; Continue shopping</a> <a class="checkout-btn" href="{{route('checkout.show')}}"><i class="fa fa-check"></i> Proceed to checkout</a> </div>
            </div>
            @else
            <h2 class="text-center mt-7 pt-5"><strong>No item in cart!</strong></h2>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>

	@endsection

	@section('scripts')
				
				
				
			
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function(){
      
      @if(Cart::count() > 0)
		  @foreach(Cart::content() as $item)
      
			$('#dec{{$item->id}}').click(function()
			{
        var data = document.getElementById('qty{{$item->id}}');
        var qid = document.getElementById('quantity{{$item->id}}');
				var val = data.value;
				var low = <?php echo $min ?>;
				if(val > low)
				{
					data.value = val - 1;
					qid.value = data.value;
				}
				else{ alert("Quantity cannot be below "+low)}

			});

			$('#inc{{$item->id}}').click(function()
			{
         var data = document.getElementById('qty{{$item->id}}');
        var qid = document.getElementById('quantity{{$item->id}}');
			 	var val = data.value;
				var high = <?php echo $item->model->quantity; ?>;
				if(val < high)
				{
					data.value = parseInt(val)+1;
					qid.value = data.value;
				}
				else{ alert("Quantity cannot be more than "+high)}
			});

      @endforeach
		  @endif
		});
	</script>
@endsection