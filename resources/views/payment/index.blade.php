@extends('layouts.master')

@section('content')
<form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
        <div class="row" style="margin-bottom:40px;">
          <div class="col-md-8 col-md-offset-2">
            <p>
            	
            	<?php
            	$getBackOriginl = explode('.',Cart::subtotal());
            	$tot = str_replace(',','',$getBackOriginl[0]).$getBackOriginl[1];
            	?>
  
                <div class="order-detail-content">
              <div class="table-responsive">
                <table class="table table-bordered cart_summary">
                  <tbody>
                    @foreach(Cart::content() as $item)
                    <tr>
                      <td class="cart_product"><a href="{{route('product.show',$item->model->productsku)}}"><img src="{{ asset('images/product').'/'.$item->model->img }}" alt="Product"></a></td>
                      <td class="cart_description"><p class="product-name"><a href="{{route('product.show',$item->model->productsku)}}">{{ $item->name}} </a></p></td>
                      <td class="price"><span>{{$item->qty}} x ₦ {{number_format($item->price,2)}}</span></td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="3"><strong>Total</strong></td>
                      <td colspan="2"><strong>₦ {{ Cart::subtotal() }} </strong></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            </p>
            <input type="hidden" name="email" value="{{Auth::user()->email}}"> {{-- required --}}
            <input type="hidden" name="order_id" value="{{session('order_id') }}">
            <input type="hidden" name="amount" value="{{$tot}}"> {{-- required in kobo --}}
            <input type="hidden" name="quantity" value="{{Cart::count()}}">
           <!--  <input type="hidden" name="metadata" value="{{ json_encode($array = ['key_name' => 'value',]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}} -->
            <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
            <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
            @csrf
            <p>
              <button class="btn btn-success btn-md btn-block" type="submit" value="Pay Now!">
              Pay Now!
              </button>
            </p>
          </div>
        </div>
</form>
@endsection