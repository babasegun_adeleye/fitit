@extends('error')

@section('title', 'Error')

@section('type', '500 - Temporary Unavailable')

@section('code', '500')

@section('message', 'Sorry, we are having a temporary problem. We have been alerted and will be rolling out a fix soon')