@extends('error')

@section('title', 'Error')

@section('type', '404 - Page not found')

@section('code', '404')

@section('message', 'We are sorry, but the page you were looking for does not exist.')