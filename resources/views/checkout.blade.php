@extends('layouts.master')

@section('scripts')
	<script src="/js/custom.js"></script>
@endsection

@section('content')
@if(Cart::count() > 0)
<div id="page">
<check-out array="{{ json_encode($cart) }}" total="{{Cart::subtotal()}}" items="{{Cart::count()}}"></check-out>
</div>
@else
<h2 class="text-center mt-7 pt-5"><strong>You need to add an Item to your cart, before you can checkout!</strong></h2>
<br><br><br><br>
@endif
@endsection