export default class Gate {

	constructor(user)
	{
		this.user = user;
	}

	isAdmin()
	{
		return this.user === 'admin'
	}

	isMerchant()
	{
		return this.user === 'merchant'
	}

	isCustomer()
	{
		return this.user === 'customer'
	}
}