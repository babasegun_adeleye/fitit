window._ = require('lodash');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.Vue = require('vue');

import { Form, HasError, AlertError, AlertSuccess } from 'vform'

import Swal from 'sweetalert2'
window.Swal = Swal

Vue.component('pagination', require('laravel-vue-pagination'));

window.Fire = new Vue()

Vue.filter('numFormat', function(value){
  let val = (value/1).toFixed(2).replace(',', '.')
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
});

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component(AlertSuccess.name, AlertSuccess)
Vue.component('home-component', require('./components/front/Home.vue').default);
Vue.component('subcategory-component', require('./components/front/Subcategory.vue').default);
Vue.component('check-out', require('./components/front/Checkout.vue').default);

const page = new Vue({
    el: '#page'
});