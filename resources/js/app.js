/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import { Form, HasError, AlertError, AlertSuccess } from 'vform'

import Swal from 'sweetalert2'
window.Swal = Swal

const objectToFormData = window.objectToFormData

Vue.component('pagination', require('laravel-vue-pagination'));

import Gate from './Gate'
Vue.prototype.$gate = new Gate(window.user)

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

import moment from 'moment'

window.Fire = new Vue()

Vue.filter('upText', function(text){
  if(text)
  {
     return text.charAt(0).toUpperCase() + text.slice(1)
  }
 
});

Vue.filter('dateFormat', function(value){
  if(value)
  {
     return moment(String(value)).format('DD/MM/YYYY')
  }
 
});

Vue.filter('numFormat', function(value){
  let val = (value/1).toFixed(2).replace(',', '.')
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
});



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('country-component', require('./components/Country.vue').default);
Vue.component('city-component', require('./components/City.vue').default);
Vue.component('category-component', require('./components/Category.vue').default);
Vue.component('subcat-component', require('./components/Subcat.vue').default);
Vue.component('product-component', require('./components/Product.vue').default);
Vue.component('product-add', require('./components/AddProduct.vue').default);
Vue.component('product-edit', require('./components/EditProduct.vue').default);
Vue.component('password', require('./components/Password.vue').default);
Vue.component('address-component', require('./components/Address.vue').default);
Vue.component('order-component', require('./components/Order.vue').default);
Vue.component('details-component', require('./components/Details.vue').default);
Vue.component('store-component', require('./components/Store.vue').default);
Vue.component('not-found', require('./components/NotFound.vue').default);
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component(AlertSuccess.name, AlertSuccess)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
let routes = [
	{ 
      name: 'country',
      path: '/dashboard/countries', 
      component: require('./components/Country.vue').default
     },

     { 
      name: 'password-change',
      path: '/dashboard/password/change', 
      component: require('./components/Password.vue').default
     },

     { 
      name: 'address',
      path: '/dashboard/address', 
      component: require('./components/Address.vue').default
     },

     { 
      name: 'product',
      path: '/dashboard/products', 
      component: require('./components/Product.vue').default
     },

     { 
      name: 'product-add',
      path: '/dashboard/product/add', 
      component: require('./components/AddProduct.vue').default
     },

     { 
      name: 'product-edit',
      path: '/dashboard/product/edit', 
      component: require('./components/EditProduct.vue').default
     },

     { 
      name: 'order',
      path: '/dashboard/orders', 
      component: require('./components/Order.vue').default
     },

     { 
      name: 'details',
      path: '/dashboard/order/details', 
      component: require('./components/Details.vue').default
     },

     { 
      name: 'store',
      path: '/dashboard/store', 
      component: require('./components/Store.vue').default
     },

     { 
      name: 'category',
      path: '/dashboard/categories', 
      component: require('./components/Category.vue').default
     },

     { 
      name: 'subcategory',
      path: '/dashboard/subcategories', 
      component: require('./components/Subcat.vue').default
     },

     { 
      name: 'city',
      path: '/dashboard/cities', 
      component: require('./components/City.vue').default
     },

     { 
      name: 'not-found',
      path: '/dashboard/*', 
      component: require('./components/NotFound.vue').default
     }
]

 const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })

const app = new Vue({
    el: '#app',
    router
});
